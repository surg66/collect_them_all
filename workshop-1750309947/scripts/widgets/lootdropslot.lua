local Widget = require "widgets/widget"
local Text = require "widgets/text"
local Image = require "widgets/image"

local LootDropSlot = Class(Widget, function(self, texture, chance, count)
    Widget._ctor(self, "LootDropSlot")

    self.bg = self:AddChild(Image("images/inventoryimages.xml", "blueprint.tex"))
    self.bg:SetSize(64, 64, 1)
    self.bg:MoveToBack()
    self.bg:SetPosition(0, 50, 0)

    if texture ~= "blueprint" then
        local atlas = GetInventoryItemAtlas(texture..".tex")
        if atlas == nil then
            atlas = "images/inventoryimages.xml"
        end

        self.icon = self:AddChild(Image(atlas, texture..".tex"))
        self.icon:SetSize(40, 40, 1)
        self.icon:SetTint(1, 1, 1, 0.8)
        self.icon:SetPosition(0, 50, 0)
    else
        self.textrnd = self:AddChild(Text(UIFONT, 35, 0))
        self.textrnd:SetHAlign(ANCHOR_LEFT)
        self.textrnd:SetString("?")
        self.textrnd:SetColour({ 1, 1, 1, 0.6 })
        self.textrnd:SetPosition(2, 45, 0)
    end

    if chance ~= nil and chance < 100 then
        self.text = self:AddChild(Text(UIFONT, 35, 0))
        self.text:SetHAlign(ANCHOR_LEFT)
        self.text:SetString(chance.."%")
        self.text:SetColour({ 1, 1, 1, 1 })
        self.text:SetPosition(10, 0, 0)
    end

    if count ~= nil then
        if count > 1 then
            self.count = self:AddChild(Text(UIFONT, 30, 0))
            self.count:SetHAlign(ANCHOR_LEFT)
            self.count:SetString(count.."x")
            self.count:SetColour({ 1, 1, 1, 1 })
            self.count:SetPosition(0, 70, 0)
        end
    end

    self:Show()
end)

function LootDropSlot:GetWidth()
    return 64
end

return LootDropSlot
