local Widget = require "widgets/widget"
local Text = require "widgets/text"
local Image = require "widgets/image"
local LootDropSlot = require "widgets/lootdropslot"

local LootDropInfo = Class(Widget, function(self)
    Widget._ctor(self, "LootDropInfo")

    self.targetprefab = ""

    self:Hide()
end)

function LootDropInfo:BuidInfo(prefab, data)
    local LOOTDROPSLOT_WIDTH = LootDropSlot:GetWidth()
    local width = 0
    local containers = {}    

    self.targetprefab = prefab

    if self.info == nil then self.info = self:AddChild(Widget("info")) end

    for i = 1, #data do
        for j = 1, #data[i] do
            local lootdrop = self.info:AddChild(LootDropSlot(data[i][j].item, data[i][j].chance, data[i][j].count))
            table.insert(containers, {inst = lootdrop, width = 55, offsety = 0})
        end
        if i < #data then
            local ortext = self.info:AddChild(Text(UIFONT, 30, 0))
            ortext:SetHAlign(ANCHOR_LEFT)
            ortext:SetString("/")
            ortext:SetColour({ 1, 1, 1, 1 })
            local otw, oth = ortext:GetSize()
            table.insert(containers, {inst = ortext, width = otw + 15, offsety = 50})
        end
    end

    for i = 1, #containers do
        containers[i].inst:SetPosition(width, containers[i].offsety, 0)

        if i < #containers then
            width = width + containers[i].width
        end
    end

    self.info:SetPosition(-width/2, 0, 0)
end

function LootDropInfo:ClearInfo()
    self.targetprefab = ""
    
    if self.info ~= nil then
        self.info:Kill()
        self.info = nil
    end
end

function LootDropInfo:SetTooltip(target, data)
    if target ~= nil and data ~= nil then
        if target.prefab ~= self.targetprefab then
            self:ClearInfo()
            self:BuidInfo(target.prefab, data)
        end
    else
        self:ClearInfo()
    end
    
    if self.targetprefab ~= "" then
        self:Show()
    else
        self:Hide()
    end
end

return LootDropInfo
