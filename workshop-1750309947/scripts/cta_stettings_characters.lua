local function WilsonPostInit(inst)
    --inst.components.builder:UnlockRecipe("razor")
    --inst.components.builder:UnlockRecipe("resurrectionstatue")
end

local function WillowPostInit(inst)
    --inst.components.builder:UnlockRecipe("coldfire")
end

local function WolfgangPostInit(inst)
end

local function WendyPostInit(inst)
end

local function Wx78PostInit(inst)
end

local function WickerbottomPostInit(inst)
    inst.components.eater.stale_hunger = TUNING.STALE_FOOD_HUNGER
    inst.components.eater.stale_health = TUNING.STALE_FOOD_HEALTH
    inst.components.eater.spoiled_hunger = TUNING.SPOILED_FOOD_HUNGER
    inst.components.eater.spoiled_health = TUNING.SPOILED_FOOD_HEALTH
    inst.components.sanity:SetMax(TUNING.WICKERBOTTOM_SANITY)
    inst.components.builder.science_bonus = 0
    inst.components.builder:UnlockRecipe("papyrus")
    inst.components.builder:UnlockRecipe("book_birds")
    inst.components.builder:UnlockRecipe("book_gardening")
    inst.components.builder:UnlockRecipe("book_brimstone")
    inst.components.builder:UnlockRecipe("book_sleep")
    inst.components.builder:UnlockRecipe("book_tentacles")
end

local function WoodiePostInit(inst)
end

local function WesPostInit(inst)
end

local function WaxwellPostInit(inst)
end

local function WathgrithrPostInit(inst)
end

local function WebberPostInit(inst)
    inst.components.builder:UnlockRecipe("razor")
    inst.components.builder:UnlockRecipe("winterhat")
end

local function WinonaPostInit(inst)

end

local function WortoxPostInit(inst)
end

return {
    ["wilson"] = WilsonPostInit,
    ["willow"] = WillowPostInit,
    ["wolfgang"] = WolfgangPostInit,
    ["wendy"] = WendyPostInit,
    ["wx78"] = Wx78PostInit,
    ["wickerbottom"] = WickerbottomPostInit,
    ["woodie"] = WoodiePostInit,
    ["wes"] = WesPostInit,
    ["waxwell"] = WaxwellPostInit,
    ["wathgrithr"] = WathgrithrPostInit,
    ["webber"] = WebberPostInit,
    ["winona"] = WinonaPostInit,
    ["wortox"] = WortoxPostInit,
}
