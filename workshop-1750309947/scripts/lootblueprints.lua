local blueprintschance = {}
local lootblueprints = {}

local function NormalizeChance(value)
    local chance = value

    if chance < 0 then chance = 0 end
    if chance > 100 then chance = 100 end

    chance = chance * 0.01

    return chance
end

local function AddLootBlueprint(inst, loot, chance)
    if inst.components.lootdropper ~= nil and chance ~= nil then        
        if loot ~= "blueprint" then
            inst.components.lootdropper:AddChanceLoot(loot.."_blueprint", NormalizeChance(chance))
        else
            inst.components.lootdropper:AddChanceLoot(loot, NormalizeChance(chance))
        end
    end
end

local function GetLootTooltip(item, chance, count)
    return {item = item, chance = chance, count = count}
end

local function PrefabSettingLootBlueprints(prefab, chance_fn, postinit_fn, forcelootdrop_fn, tooltip_fn)
    lootblueprints[prefab] = {chance_fn = chance_fn, postinit_fn = postinit_fn, forcelootdrop_fn = forcelootdrop_fn, tooltip_fn = tooltip_fn}
end

-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("butterfly",
    function()
        blueprintschance["butterfly_saddle_race"] = 2
        blueprintschance["butterfly_saddle_war"] = 2
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "saddle_race", blueprintschance["butterfly_saddle_race"]) -- ��������� �����
        else
            AddLootBlueprint(inst, "saddle_war", blueprintschance["butterfly_saddle_war"]) -- ������ �����
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("saddle_race", blueprintschance["butterfly_saddle_race"], 1)})
        table.insert(tooltip, {GetLootTooltip("saddle_war", blueprintschance["butterfly_saddle_war"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������� �����
PrefabSettingLootBlueprints("deer",
    function()
        blueprintschance["deer_waxpaper"] = 10
        blueprintschance["deer_cane"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "waxpaper", blueprintschance["deer_waxpaper"]) -- �������� ������
        else
            AddLootBlueprint(inst, "cane", blueprintschance["deer_cane"]) -- ������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("waxpaper", blueprintschance["deer_waxpaper"], 1)})
        table.insert(tooltip, {GetLootTooltip("cane", blueprintschance["deer_cane"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������
PrefabSettingLootBlueprints("beefalo",
    function()
        blueprintschance["beefalo_beefalohat"] = 10
        blueprintschance["beefalo_turf_carpetfloor"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "beefalohat", blueprintschance["beefalo_beefalohat"]) -- ����� ������
        else
            AddLootBlueprint(inst, "turf_carpetfloor", blueprintschance["beefalo_turf_carpetfloor"]) -- �����
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("beefalohat", blueprintschance["beefalo_beefalohat"], 1)})
        table.insert(tooltip, {GetLootTooltip("turf_carpetfloor", blueprintschance["beefalo_turf_carpetfloor"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����
PrefabSettingLootBlueprints("warg",
    function()
        blueprintschance["warg_trap_teeth"] = 50
        blueprintschance["warg_blueprint"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["warg_trap_teeth"]) then
                inst.components.lootdropper:SpawnLootPrefab("trap_teeth_blueprint") -- ��������� �������
            end
            if math.random() <= NormalizeChance(blueprintschance["warg_blueprint"]) then
                inst.components.lootdropper:SpawnLootPrefab("blueprint")
            end
        end        
    end,    
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("trap_teeth", blueprintschance["warg_trap_teeth"], 1),
                               GetLootTooltip("blueprint", blueprintschance["warg_blueprint"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �����-����
PrefabSettingLootBlueprints("lightninggoat",
    function()
        blueprintschance["lightninggoat_lightning_rod"] = 15
        blueprintschance["lightninggoat_transistor"] = 15
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "lightning_rod", blueprintschance["lightninggoat_lightning_rod"]) -- ����������
        else
            AddLootBlueprint(inst, "transistor", blueprintschance["lightninggoat_transistor"]) -- ������������� ���������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("lightning_rod", blueprintschance["lightninggoat_lightning_rod"], 1)})
        table.insert(tooltip, {GetLootTooltip("transistor", blueprintschance["lightninggoat_transistor"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �����
PrefabSettingLootBlueprints("crow",
    function()
        blueprintschance["crow_featherpencil"] = 5
        blueprintschance["crow_featherhat"] = 5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "featherpencil", blueprintschance["crow_featherpencil"]) -- �������� ��������
        else
            AddLootBlueprint(inst, "featherhat", blueprintschance["crow_featherhat"]) -- �������� �����
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("featherpencil", blueprintschance["crow_featherpencil"], 1)})
        table.insert(tooltip, {GetLootTooltip("featherhat", blueprintschance["crow_featherhat"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������� �����
PrefabSettingLootBlueprints("tallbird",
    function()
        blueprintschance["tallbird_birdcage"] = 10
    end,
    function(inst)
        AddLootBlueprint(inst, "birdcage", blueprintschance["tallbird_birdcage"]) -- ������ ������
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("birdcage", blueprintschance["tallbird_birdcage"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������� ������
PrefabSettingLootBlueprints("grassgekko",
    function()
        blueprintschance["grassgekko_boomerang"] = 10
        blueprintschance["grassgekko_blowdart_sleep"] = 10
        blueprintschance["grassgekko_steeringwheel_item"] = 10
    end,
    function(inst)
        local rnd = math.random()
        if rnd >= 0 and rnd <= 0.33 then
            AddLootBlueprint(inst, "boomerang", blueprintschance["grassgekko_boomerang"]) -- ��������
        elseif rnd > 0.33 and rnd <= 0.66 then
            AddLootBlueprint(inst, "blowdart_sleep", blueprintschance["grassgekko_blowdart_sleep"]) -- ���������� ������
        else
            AddLootBlueprint(inst, "steeringwheel_item", blueprintschance["grassgekko_steeringwheel_item"]) -- ����� ��� ��������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("boomerang", blueprintschance["grassgekko_boomerang"], 1)})
        table.insert(tooltip, {GetLootTooltip("blowdart_sleep", blueprintschance["grassgekko_blowdart_sleep"], 1)})
        table.insert(tooltip, {GetLootTooltip("steeringwheel_item", blueprintschance["grassgekko_steeringwheel_item"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������� ��������
PrefabSettingLootBlueprints("tentacle_pillar",
    function()
        blueprintschance["tentacle_pillar_gunpowder"] = 10
    end,
    function(inst)
        AddLootBlueprint(inst, "gunpowder", blueprintschance["tentacle_pillar_gunpowder"]) -- �����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("gunpowder", blueprintschance["tentacle_pillar_gunpowder"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������������ (���������)
PrefabSettingLootBlueprints("lureplant",
    function()
        blueprintschance["lureplant_slow_farmplot"] = 10
        blueprintschance["lureplant_fast_farmplot"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "slow_farmplot", blueprintschance["lureplant_slow_farmplot"]) -- ������� ������
        else
            AddLootBlueprint(inst, "fast_farmplot", blueprintschance["lureplant_fast_farmplot"]) -- ���������� ������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("slow_farmplot", blueprintschance["lureplant_slow_farmplot"], 1)})
        table.insert(tooltip, {GetLootTooltip("fast_farmplot", blueprintschance["lureplant_fast_farmplot"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("glommer",
    function()
        blueprintschance["glommer_marblebean"] = 33.3
    end,
    function(inst)
        AddLootBlueprint(inst, "marblebean", blueprintschance["glommer_marblebean"]) -- ��������� ���
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("marblebean", blueprintschance["glommer_marblebean"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������� ����
PrefabSettingLootBlueprints("spider_dropper",
    function()
        blueprintschance["spider_dropper_sewing_kit"] = 5
    end,
    function(inst)
        AddLootBlueprint(inst, "sewing_kit", blueprintschance["spider_dropper_sewing_kit"]) -- ����� ��� �����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("sewing_kit", blueprintschance["spider_dropper_sewing_kit"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������� �����
PrefabSettingLootBlueprints("worm",
    function()
        blueprintschance["worm_coldfirepit"] = 10
        blueprintschance["worm_minerhat"] = 10
        blueprintschance["worm_fake_sentryward"] = 5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "coldfirepit", blueprintschance["worm_coldfirepit"]) -- ��������������� ��������
        else
            AddLootBlueprint(inst, "minerhat", blueprintschance["worm_minerhat"]) -- ��������� �����
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        if not inst:HasTag("lure") then
            table.insert(tooltip, {GetLootTooltip("coldfirepit", blueprintschance["worm_coldfirepit"], 1)})
            table.insert(tooltip, {GetLootTooltip("minerhat", blueprintschance["worm_minerhat"], 1)})
        else
            table.insert(tooltip, {GetLootTooltip("sentryward", blueprintschance["worm_fake_sentryward"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������
PrefabSettingLootBlueprints("hound",
    function()
        blueprintschance["hound_sweatervest"] = 5
        blueprintschance["hound_razor"] = 5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "sweatervest", blueprintschance["hound_sweatervest"]) -- ������� �����
        else
            AddLootBlueprint(inst, "razor", blueprintschance["hound_razor"]) -- ������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("sweatervest", blueprintschance["hound_sweatervest"], 1)})
        table.insert(tooltip, {GetLootTooltip("razor", blueprintschance["hound_razor"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������� ����
PrefabSettingLootBlueprints("dragonfly",
    function()
        blueprintschance["dragonfly_armordragonfly"] = 100
        blueprintschance["dragonfly_dragonflychest"] = 100
        blueprintschance["dragonfly_turf_dragonfly"] = 100
        blueprintschance["dragonfly_blueprints"] = 100
    end,
    nil,
    function(inst)       
        if inst.components.lootdropper ~= nil then
            local chance = math.random()
            if chance <= 0.33 then
                if math.random() <= NormalizeChance(blueprintschance["dragonfly_armordragonfly"]) then
                    inst.components.lootdropper:SpawnLootPrefab("armordragonfly_blueprint") -- ���������� �����
                end
            end
            if chance > 0.33 and chance <= 0.66 then
                if math.random() <= NormalizeChance(blueprintschance["dragonfly_dragonflychest"]) then
                    inst.components.lootdropper:SpawnLootPrefab("dragonflychest_blueprint") -- ���������� ������
                end
            end
            if chance > 0.66 then
                if math.random() <= NormalizeChance(blueprintschance["dragonfly_turf_dragonfly"]) then
                    inst.components.lootdropper:SpawnLootPrefab("turf_dragonfly_blueprint") -- ���������� ���
                end
            end
            if math.random() <= NormalizeChance(blueprintschance["dragonfly_blueprints"]) then
                inst.components.lootdropper:SpawnLootPrefab("blueprint")
                inst.components.lootdropper:SpawnLootPrefab("blueprint")
            end
        end    
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("armordragonfly", blueprintschance["dragonfly_armordragonfly"], 1),
                               GetLootTooltip("blueprint", blueprintschance["dragonfly_blueprints"], 2)})
        table.insert(tooltip, {GetLootTooltip("dragonflychest", blueprintschance["dragonfly_dragonflychest"], 1),
                               GetLootTooltip("blueprint", blueprintschance["dragonfly_blueprints"], 2)})
        table.insert(tooltip, {GetLootTooltip("turf_dragonfly", blueprintschance["dragonfly_turf_dragonfly"], 1),
                               GetLootTooltip("blueprint", blueprintschance["dragonfly_blueprints"], 2)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������� �����
PrefabSettingLootBlueprints("minotaur",
    function()
        blueprintschance["minotaur_cookpot"] = 100
        blueprintschance["minotaur_blueprints"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["minotaur_cookpot"]) then
                inst.components.lootdropper:SpawnLootPrefab("cookpot_blueprint") -- �����
            end
            if math.random() <= NormalizeChance(blueprintschance["minotaur_blueprints"]) then
                for i = 1, 4 do
                    inst.components.lootdropper:SpawnLootPrefab("blueprint")
                end
            end
        end
    end,    
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("cookpot", blueprintschance["minotaur_cookpot"], 1),
                               GetLootTooltip("blueprint", blueprintschance["minotaur_blueprints"], 4)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������
PrefabSettingLootBlueprints("catcoon",
    function()
        blueprintschance["catcoon_whip"] = 5
        blueprintschance["catcoon_catcoonhat"] = 5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "whip", blueprintschance["catcoon_whip"]) -- �����-�����������
        else
            AddLootBlueprint(inst, "catcoonhat", blueprintschance["catcoon_catcoonhat"]) -- ������� �����
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("whip", blueprintschance["catcoon_whip"], 1)})
        table.insert(tooltip, {GetLootTooltip("catcoonhat", blueprintschance["catcoon_catcoonhat"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����-�������
PrefabSettingLootBlueprints("toadstool",
    function()
        blueprintschance["toadstool_icebox"] = 100
        blueprintschance["toadstool_blueprints"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["toadstool_icebox"]) then
                inst.components.lootdropper:SpawnLootPrefab("icebox_blueprint") -- �����������
            end
            if math.random() <= NormalizeChance(blueprintschance["toadstool_blueprints"]) then
                for i = 1, 4 do
                    inst.components.lootdropper:SpawnLootPrefab("blueprint")
                end
            end
        end
    end,    
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("icebox", blueprintschance["toadstool_icebox"], 1),
                               GetLootTooltip("blueprint", blueprintschance["toadstool_blueprints"], 4)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����/����������
PrefabSettingLootBlueprints("bunnyman",
    function()
        blueprintschance["bunnyman_bedroll_furry"] = 10
        blueprintschance["bunnyman_rabbithouse"] = 5
        blueprintschance["bunnyman_pitchfork"] = 5
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            local chance = math.random()
            if chance <= 0.33 then
                if math.random() <= NormalizeChance(blueprintschance["bunnyman_bedroll_furry"]) then
                    inst.components.lootdropper:SpawnLootPrefab("bedroll_furry_blueprint") -- ������� ��������
                end
            end
            if chance > 0.33 and chance <= 0.66 then
                if math.random() <= NormalizeChance(blueprintschance["bunnyman_rabbithouse"]) then
                    inst.components.lootdropper:SpawnLootPrefab("rabbithouse_blueprint") -- ������ �����
                end
            end
            if chance > 0.66 then
                if math.random() <= NormalizeChance(blueprintschance["bunnyman_pitchfork"]) then
                    inst.components.lootdropper:SpawnLootPrefab("pitchfork_blueprint") -- ����
                end
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("bedroll_furry", blueprintschance["bunnyman_bedroll_furry"], 1)})
        table.insert(tooltip, {GetLootTooltip("rabbithouse", blueprintschance["bunnyman_rabbithouse"], 1)})
        table.insert(tooltip, {GetLootTooltip("pitchfork", blueprintschance["bunnyman_pitchfork"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �����
PrefabSettingLootBlueprints("perd",
    function()
        blueprintschance["perd_saddle_basic"] = 10
    end,
    function(inst)
        AddLootBlueprint(inst, "saddle_basic", blueprintschance["perd_saddle_basic"]) -- �����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("saddle_basic", blueprintschance["perd_saddle_basic"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������� �������
PrefabSettingLootBlueprints("rocky",
    function()
        blueprintschance["rocky_heatrock"] = 25
        blueprintschance["rocky_anchor_item"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "heatrock", blueprintschance["rocky_heatrock"]) -- ���������� ������
        else
            AddLootBlueprint(inst, "anchor_item", blueprintschance["rocky_anchor_item"]) -- ����� ��� �����
        end
        
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("heatrock", blueprintschance["rocky_heatrock"], 1)})
        table.insert(tooltip, {GetLootTooltip("anchor_item", blueprintschance["rocky_anchor_item"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������
PrefabSettingLootBlueprints("canary",
    function()
        blueprintschance["canary_blowdart_yellow"] = 20
    end,
    function(inst)
        AddLootBlueprint(inst, "blowdart_yellow", blueprintschance["canary_blowdart_yellow"]) -- ������������� ������
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("blowdart_yellow", blueprintschance["canary_blowdart_yellow"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �����
PrefabSettingLootBlueprints("buzzard",
    function()
        blueprintschance["buzzard_fence_gate_item"] = 10
        blueprintschance["buzzard_birdcage"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "fence_gate_item", blueprintschance["buzzard_fence_gate_item"]) -- ���������� ������
        else
            AddLootBlueprint(inst, "birdcage", blueprintschance["buzzard_birdcage"]) -- ������ ������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("fence_gate_item", blueprintschance["buzzard_fence_gate_item"], 1)})
        table.insert(tooltip, {GetLootTooltip("birdcage", blueprintschance["buzzard_birdcage"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �����
PrefabSettingLootBlueprints("klaus",
    function()
        blueprintschance["klaus_backpack"] = 100
        blueprintschance["klaus_blueprints"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["klaus_backpack"]) then            
                inst.components.lootdropper:SpawnLootPrefab("backpack_blueprint") -- ������
            end
            if math.random() <= NormalizeChance(blueprintschance["klaus_blueprints"]) then
                for i = 1, 4 do
                    inst.components.lootdropper:SpawnLootPrefab("blueprint")
                end
            end
        end
    end,    
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("backpack", blueprintschance["klaus_backpack"], 1),
                               GetLootTooltip("blueprint", blueprintschance["klaus_blueprints"], 4)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������/�������� ���������
local function KoalefantChance()
    blueprintschance["koalefant_trunkvest_summer"] = 20
    blueprintschance["koalefant_trunkvest_winter"] = 20
end

local function KoalefantLoot(inst)
    if math.random() < 0.5 then
        AddLootBlueprint(inst, "trunkvest_summer", blueprintschance["koalefant_trunkvest_summer"]) -- ���������
    else
        AddLootBlueprint(inst, "trunkvest_winter", blueprintschance["koalefant_trunkvest_winter"]) -- ���������� ���������
    end
end

local function KoalefantTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("trunkvest_summer", blueprintschance["koalefant_trunkvest_summer"], 1)})
    table.insert(tooltip, {GetLootTooltip("trunkvest_winter", blueprintschance["koalefant_trunkvest_winter"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("koalefant_summer", KoalefantChance, KoalefantLoot, nil, KoalefantTooltip)
PrefabSettingLootBlueprints("koalefant_winter", KoalefantChance, KoalefantLoot, nil, KoalefantTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- �����
PrefabSettingLootBlueprints("mosquito",
    function()
        blueprintschance["mosquito_waterballoon"] = 5
    end,
    function(inst)
        AddLootBlueprint(inst, "waterballoon", blueprintschance["mosquito_waterballoon"]) -- ����� � �����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("waterballoon", blueprintschance["mosquito_waterballoon"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������� ������
PrefabSettingLootBlueprints("spiderqueen",
    function()
        blueprintschance["spiderqueen_tent"] = 20
        blueprintschance["spiderqueen_blueprint"] = 50
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["spiderqueen_tent"]) then            
                inst.components.lootdropper:SpawnLootPrefab("tent_blueprint") -- �������
            end
            if math.random() <= NormalizeChance(blueprintschance["spiderqueen_blueprint"]) then
                inst.components.lootdropper:SpawnLootPrefab("blueprint")
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("tent", blueprintschance["spiderqueen_tent"], 1),
                               GetLootTooltip("blueprint", blueprintschance["spiderqueen_blueprint"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������� ��������/��������� ��������
PrefabSettingLootBlueprints("monkey",
    function()
        blueprintschance["monkey_tophat"] = 5
        blueprintschance["monkey_nightstick"] = 5
        blueprintschance["monkey_nightlight"] = 5
    end,
    function(inst)
        inst:ListenForEvent("death", function(inst)
            if inst.components.lootdropper ~= nil then
                if inst:HasTag("nightmare") then
                    if math.random() <= NormalizeChance(blueprintschance["monkey_nightlight"]) then
                        inst.components.lootdropper:SpawnLootPrefab("nightlight_blueprint") -- ����� ����
                    end
                else
                    if math.random() < 0.5 then
                        if math.random() <= NormalizeChance(blueprintschance["monkey_tophat"]) then
                            inst.components.lootdropper:SpawnLootPrefab("tophat_blueprint") -- �������
                        end
                    else
                        if math.random() <= NormalizeChance(blueprintschance["monkey_nightstick"]) then
                            inst.components.lootdropper:SpawnLootPrefab("nightstick_blueprint") -- �����������
                        end
                    end
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("nightmare") then
            table.insert(tooltip, {GetLootTooltip("nightlight", blueprintschance["monkey_nightlight"], 1)})
        else
            table.insert(tooltip, {GetLootTooltip("tophat", blueprintschance["monkey_tophat"], 1)})
            table.insert(tooltip, {GetLootTooltip("nightstick", blueprintschance["monkey_nightstick"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("krampus",
    function()
        blueprintschance["krampus_onemanband"] = 20
    end,
    function(inst)
        AddLootBlueprint(inst, "onemanband", blueprintschance["krampus_onemanband"]) -- �������-�������
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("onemanband", blueprintschance["krampus_onemanband"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������� ������
PrefabSettingLootBlueprints("firehound",
    function()
        blueprintschance["firehound_firestaff"] = 5
    end,
    function(inst)
        AddLootBlueprint(inst, "firestaff", blueprintschance["firehound_firestaff"]) -- �������� �����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("firestaff", blueprintschance["firehound_firestaff"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������� �����
PrefabSettingLootBlueprints("robin",
    function()
        blueprintschance["robin_blowdart_fire"] = 5
        blueprintschance["robin_scarecrow"] = 5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "blowdart_fire", blueprintschance["robin_blowdart_fire"]) -- �������� ������
        else
            AddLootBlueprint(inst, "scarecrow", blueprintschance["robin_scarecrow"]) -- ����������� ������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("blowdart_fire", blueprintschance["robin_blowdart_fire"], 1)})
        table.insert(tooltip, {GetLootTooltip("scarecrow", blueprintschance["robin_scarecrow"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������/����������
PrefabSettingLootBlueprints("rabbit",
    function()
        blueprintschance["rabbit_earmuffshat"] = 5
        blueprintschance["rabbit_nightmarefuel"] = 5
        blueprintschance["rabbit_resurrectionstatue"] = 5
    end,
    function(inst)
        if inst.components.lootdropper ~= nil then
            local chance = math.random()
            if chance <= 0.33 then
                if math.random() <= NormalizeChance(blueprintschance["rabbit_earmuffshat"]) then
                    inst.components.lootdropper:SpawnLootPrefab("earmuffshat_blueprint") -- ��������
                end
            end
            if chance > 0.33 and chance <= 0.66 then
                if math.random() <= NormalizeChance(blueprintschance["rabbit_nightmarefuel"]) then
                    inst.components.lootdropper:SpawnLootPrefab("nightmarefuel_blueprint") -- ������� �����
                end
            end
            if chance > 0.66 then
                if math.random() <= NormalizeChance(blueprintschance["rabbit_resurrectionstatue"]) then
                    inst.components.lootdropper:SpawnLootPrefab("resurrectionstatue_blueprint") -- ������ ������
                end
            end
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("earmuffshat", blueprintschance["rabbit_earmuffshat"], 1)})
        table.insert(tooltip, {GetLootTooltip("nightmarefuel", blueprintschance["rabbit_nightmarefuel"], 1)})
        table.insert(tooltip, {GetLootTooltip("resurrectionstatue", blueprintschance["rabbit_resurrectionstatue"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����������
PrefabSettingLootBlueprints("mole",
    function()
        blueprintschance["mole_molehat"] = 5
    end,
    function(inst)
        AddLootBlueprint(inst, "molehat", blueprintschance["robin_scarecrow"]) -- �����������
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("molehat", blueprintschance["mole_molehat"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������� ���
PrefabSettingLootBlueprints("deciduoustree",
    function()
        blueprintschance["deciduoustree_wardrobe"] = 20
        blueprintschance["deciduoustree_boards"] = 1
        blueprintschance["deciduoustree_turf_woodfloor"] = 1
    end,
    function(inst)
        inst:ListenForEvent("workfinished", function(inst, data)
            if inst.components.lootdropper ~= nil then
                if inst:HasTag("monster") then
                    if math.random() <= NormalizeChance(blueprintschance["deciduoustree_wardrobe"]) then
                        inst.components.lootdropper:SpawnLootPrefab("deciduoustree_blueprint") -- �������� ����
                    end
                else
                    if math.random() < 0.5 then
                        if math.random() <= NormalizeChance(blueprintschance["deciduoustree_boards"]) then
                            inst.components.lootdropper:SpawnLootPrefab("boards_blueprint") -- �����
                        end
                    else
                        if math.random() <= NormalizeChance(blueprintschance["deciduoustree_turf_woodfloor"]) then
                            inst.components.lootdropper:SpawnLootPrefab("turf_woodfloor_blueprint") -- ������
                        end
                    end
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if not inst:HasTag("stump") then
            if inst:HasTag("monster") then
                table.insert(tooltip, {GetLootTooltip("wardrobe", blueprintschance["deciduoustree_wardrobe"], 1)})
            else
                table.insert(tooltip, {GetLootTooltip("boards", blueprintschance["deciduoustree_boards"], 1)})
                table.insert(tooltip, {GetLootTooltip("turf_woodfloor", blueprintschance["deciduoustree_turf_woodfloor"], 1)})
            end
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������
PrefabSettingLootBlueprints("mossling",
    function()
        blueprintschance["mossling_rainhat"] = 10
        blueprintschance["mossling_raincoat"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "rainhat", blueprintschance["mossling_rainhat"]) -- �������� �����
        else
            AddLootBlueprint(inst, "raincoat", blueprintschance["mossling_raincoat"]) -- ��������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("rainhat", blueprintschance["mossling_rainhat"], 1)})
        table.insert(tooltip, {GetLootTooltip("raincoat", blueprintschance["mossling_raincoat"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����/����
PrefabSettingLootBlueprints("moose",
    function()
        blueprintschance["moose_featherfan"] = 50
        blueprintschance["moose_staff_tornado"] = 50
        blueprintschance["moose_oar_driftwood"] = 5
        blueprintschance["moose_blueprints"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
        
            local chance = math.random()
            if chance <= 0.33 then
                if math.random() <= NormalizeChance(blueprintschance["moose_featherfan"]) then
                    inst.components.lootdropper:SpawnLootPrefab("featherfan_blueprint") -- ��������� ����
                end
            end
            if chance > 0.33 and chance <= 0.66 then
                if math.random() <= NormalizeChance(blueprintschance["moose_staff_tornado"]) then
                    inst.components.lootdropper:SpawnLootPrefab("staff_tornado_blueprint") -- ��������
                end
            end
            if chance > 0.66 then
                if math.random() <= NormalizeChance(blueprintschance["moose_oar_driftwood"]) then
                    inst.components.lootdropper:SpawnLootPrefab("oar_driftwood_blueprint") -- ����� �� ������
                end
            end            
            if math.random() <= NormalizeChance(blueprintschance["moose_blueprints"]) then
                inst.components.lootdropper:SpawnLootPrefab("blueprint")
                inst.components.lootdropper:SpawnLootPrefab("blueprint")
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("featherfan", blueprintschance["moose_featherfan"], 1),
                               GetLootTooltip("blueprint", blueprintschance["moose_blueprints"], 2)})
        table.insert(tooltip, {GetLootTooltip("staff_tornado", blueprintschance["moose_staff_tornado"], 1),
                               GetLootTooltip("blueprint", blueprintschance["moose_blueprints"], 2)})
        table.insert(tooltip, {GetLootTooltip("oar_driftwood", blueprintschance["moose_oar_driftwood"], 1),
                               GetLootTooltip("blueprint", blueprintschance["moose_blueprints"], 2)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("frog",
    function()
        blueprintschance["frog_umbrella"] = 5
        blueprintschance["frog_fishingrod"] = 5
        blueprintschance["frog_oceanfishingrod"] = 1
    end,
    function(inst)
        local chance = math.random()
        if chance <= 0.33 then
            AddLootBlueprint(inst, "umbrella", blueprintschance["frog_umbrella"]) -- ������
        end
        if chance > 0.33 and chance <= 0.66 then
            AddLootBlueprint(inst, "fishingrod", blueprintschance["frog_fishingrod"]) -- ������
        end
        if chance > 0.66 then
            AddLootBlueprint(inst, "oceanfishingrod", blueprintschance["frog_oceanfishingrod"]) -- ��������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("umbrella", blueprintschance["frog_umbrella"], 1)})
        table.insert(tooltip, {GetLootTooltip("fishingrod", blueprintschance["frog_fishingrod"], 1)})
        table.insert(tooltip, {GetLootTooltip("oceanfishingrod", blueprintschance["frog_oceanfishingrod"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������
PrefabSettingLootBlueprints("walrus",
    function()
        blueprintschance["walrus_cane"] = 20
    end,
    function(inst)
        AddLootBlueprint(inst, "cane", blueprintschance["walrus_cane"]) -- ������
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("cane", blueprintschance["walrus_cane"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������� �������
PrefabSettingLootBlueprints("little_walrus",
    function()
        blueprintschance["little_walrus_cane"] = 20
        blueprintschance["little_walrus_brush"] = 20
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "cane", blueprintschance["little_walrus_cane"]) -- ������
        else
            AddLootBlueprint(inst, "brush", blueprintschance["little_walrus_brush"]) -- ٸ���
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("cane", blueprintschance["little_walrus_cane"], 1)})
        table.insert(tooltip, {GetLootTooltip("brush", blueprintschance["little_walrus_brush"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������-������
PrefabSettingLootBlueprints("bearger",
    function()
        blueprintschance["bearger_icepack"] = 50
        blueprintschance["bearger_beargervest"] = 50
        blueprintschance["bearger_blueprints"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["bearger_icepack"]) then
                    inst.components.lootdropper:SpawnLootPrefab("icepack_blueprint") -- �����������
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["bearger_beargervest"]) then
                    inst.components.lootdropper:SpawnLootPrefab("beargervest_blueprint") -- ����� ���������
                end
            end
            if math.random() <= NormalizeChance(blueprintschance["bearger_blueprints"]) then
                for i = 1, 3 do
                    inst.components.lootdropper:SpawnLootPrefab("blueprint")
                end
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("icepack", blueprintschance["bearger_icepack"], 1),
                               GetLootTooltip("blueprint", blueprintschance["bearger_blueprints"], 3)})
        table.insert(tooltip, {GetLootTooltip("beargervest", blueprintschance["bearger_beargervest"], 1),
                               GetLootTooltip("blueprint", blueprintschance["bearger_blueprints"], 3)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������������ ����/�����/���� + ����������� ������
local function MechanismsChance(inst)
    blueprintschance["mechanisms_purplegem"] = 10
    blueprintschance["mechanisms_turf_checkerfloor"] = 10
end

local function MechanismsLoot(inst)
    if math.random() < 0.5 then
        AddLootBlueprint(inst, "purplegem", blueprintschance["mechanisms_purplegem"]) -- ���������� ��������
    else
        AddLootBlueprint(inst, "turf_checkerfloor", blueprintschance["mechanisms_turf_checkerfloor"]) -- ��������� ���
    end
end

local function MechanismsTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("purplegem", blueprintschance["mechanisms_purplegem"], 1)})
    table.insert(tooltip, {GetLootTooltip("turf_checkerfloor", blueprintschance["mechanisms_turf_checkerfloor"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("knight", MechanismsChance, MechanismsLoot, nil, MechanismsTooltip)
PrefabSettingLootBlueprints("knight_nightmare", MechanismsChance, MechanismsLoot, nil, MechanismsTooltip)
PrefabSettingLootBlueprints("rook", MechanismsChance, MechanismsLoot, nil, MechanismsTooltip)
PrefabSettingLootBlueprints("rook_nightmare", MechanismsChance, MechanismsLoot, nil, MechanismsTooltip)
PrefabSettingLootBlueprints("bishop", MechanismsChance, MechanismsLoot, nil, MechanismsTooltip)
PrefabSettingLootBlueprints("bishop_nightmare", MechanismsChance, MechanismsLoot, nil, MechanismsTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ���������� ���
PrefabSettingLootBlueprints("antlion",
    function()
        blueprintschance["antlion_firesuppressor"] = 100
        blueprintschance["antlion_blueprints"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["antlion_firesuppressor"]) then
                inst.components.lootdropper:SpawnLootPrefab("firesuppressor_blueprint") -- ������������ ������
            end
            if math.random() <= NormalizeChance(blueprintschance["antlion_blueprints"]) then
                inst.components.lootdropper:SpawnLootPrefab("blueprint")
                inst.components.lootdropper:SpawnLootPrefab("blueprint")
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("firesuppressor", blueprintschance["firesuppressor_blueprint"], 1),
                               GetLootTooltip("blueprint", blueprintschance["antlion_blueprints"], 2)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������
PrefabSettingLootBlueprints("bat",
    function()
        blueprintschance["bat_batbat"] = 5
    end,
    function(inst)
        AddLootBlueprint(inst, "batbat", blueprintschance["bat_batbat"]) -- ������� ����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("batbat", blueprintschance["bat_batbat"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����
PrefabSettingLootBlueprints("merm",
    function()
        blueprintschance["merm_lifeinjector"] = 5
        blueprintschance["merm_tacklestation"] = 1
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "lifeinjector", blueprintschance["merm_lifeinjector"]) -- ����������������� ��������
        else
            AddLootBlueprint(inst, "tacklestation", blueprintschance["merm_tacklestation"]) -- ���� ��� �������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("lifeinjector", blueprintschance["merm_lifeinjector"], 1)})
        table.insert(tooltip, {GetLootTooltip("tacklestation", blueprintschance["merm_tacklestation"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������� ������
PrefabSettingLootBlueprints("stalker_atrium",
    function()
        blueprintschance["stalker_watermelonhat"] = 100
        blueprintschance["stalker_blueprints"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["stalker_watermelonhat"]) then
                inst.components.lootdropper:SpawnLootPrefab("watermelonhat_blueprint") -- ������ �����
            end
            if math.random() <= NormalizeChance(blueprintschance["stalker_blueprints"]) then
                for i = 1, 6 do
                    inst.components.lootdropper:SpawnLootPrefab("blueprint")
                end
            end
        end
    end,    
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("watermelonhat", blueprintschance["stalker_watermelonhat"], 1),
                               GetLootTooltip("blueprint", blueprintschance["stalker_blueprints"], 6)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����
PrefabSettingLootBlueprints("spider",
    function()
        blueprintschance["spider_birdtrap"] = 5
        blueprintschance["spider_beehat"] = 5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "birdtrap", blueprintschance["spider_birdtrap"]) -- ������� ��� ����
        else
            AddLootBlueprint(inst, "beehat", blueprintschance["spider_beehat"]) -- ����� ���������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("birdtrap", blueprintschance["spider_birdtrap"], 1)})
        table.insert(tooltip, {GetLootTooltip("beehat", blueprintschance["spider_beehat"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����-����
PrefabSettingLootBlueprints("spider_warrior",
    function()
        blueprintschance["spider_warrior_minisign_item"] = 10
    end,
    function(inst)
         AddLootBlueprint(inst, "minisign_item", blueprintschance["spider_warrior_minisign_item"]) -- ����-��������
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("minisign_item", blueprintschance["spider_warrior_minisign_item"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������
PrefabSettingLootBlueprints("penguin",
    function()
        blueprintschance["penguin_icehat"] = 5
    end,
    function(inst)
        AddLootBlueprint(inst, "icehat", blueprintschance["penguin_icehat"]) -- ������� ���
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("icehat", blueprintschance["penguin_icehat"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������ / �������� ����
local function SpiderCaveChance()
    blueprintschance["spidercave_healingsalve"] = 5
end

local function SpiderCavelingLoot(inst)
    AddLootBlueprint(inst, "healingsalve", blueprintschance["spidercave_healingsalve"]) -- �������� ����
end

local function SpiderCaveTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("healingsalve", blueprintschance["spidercave_healingsalve"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("spider_spitter", SpiderCaveChance, SpiderCavelingLoot, nil, SpiderCaveTooltip)
PrefabSettingLootBlueprints("spider_hider", SpiderCaveChance, SpiderCavelingLoot, nil, SpiderCaveTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- �������� ���� (�������)/�������� ���� (���������)
local function CrawlingChance()
    blueprintschance["crawling_purpleamulet"] = 5
end

local function CrawlingLoot(inst)
    AddLootBlueprint(inst, "purpleamulet", blueprintschance["crawling_purpleamulet"]) -- ������ ��������
end

local function CrawlingTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("purpleamulet", blueprintschance["crawling_purpleamulet"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("crawlinghorror", CrawlingChance, CrawlingLoot, nil, CrawlingTooltip)
PrefabSettingLootBlueprints("crawlingnightmare", CrawlingChance, CrawlingLoot, nil, CrawlingTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("ghost",
    function()
        blueprintschance["ghost_telestaff"] = 20
        blueprintschance["ghost_telebase"] = 20
    end,
    function(inst)
        if inst.components.lootdropper == nil then
            inst:AddComponent("lootdropper")
        end        
        inst:ListenForEvent("death", function(inst)
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["ghost_telestaff"]) then
                    inst.components.lootdropper:SpawnLootPrefab("telestaff_blueprint") -- ����� �����������
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["ghost_telebase"]) then
                    inst.components.lootdropper:SpawnLootPrefab("telebase_blueprint") -- ����� �����������
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("telestaff", blueprintschance["ghost_telestaff"], 1)})
        table.insert(tooltip, {GetLootTooltip("telebase", blueprintschance["ghost_telebase"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������� ����������� ����
PrefabSettingLootBlueprints("birchnutdrake",
    function()
        blueprintschance["birchnutdrake_bedroll_straw"] = 5
        blueprintschance["birchnutdrake_arrow_sign"] = 5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "bedroll_straw", blueprintschance["birchnutdrake_bedroll_straw"]) -- ��������
        else
            AddLootBlueprint(inst, "arrow_sign", blueprintschance["birchnutdrake_arrow_sign"]) -- ���������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("bedroll_straw", blueprintschance["birchnutdrake_bedroll_straw"], 1)})
        table.insert(tooltip, {GetLootTooltip("arrow_sign", blueprintschance["birchnutdrake_arrow_sign"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("slurper",
    function()
        blueprintschance["slurper_armorslurper"] = 5
        blueprintschance["slurper_boatpatch"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "armorslurper", blueprintschance["slurper_armorslurper"]) -- ���� �������
        else
            AddLootBlueprint(inst, "boatpatch", blueprintschance["slurper_boatpatch"]) -- �������� ��������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("armorslurper", blueprintschance["slurper_armorslurper"], 1)})
        table.insert(tooltip, {GetLootTooltip("boatpatch", blueprintschance["slurper_boatpatch"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �����
PrefabSettingLootBlueprints("bee",
    function()
        blueprintschance["bee_beebox"] = 2.5
        blueprintschance["bee_bandage"] = 2.5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "beebox", blueprintschance["bee_beebox"]) -- ����
        else
            AddLootBlueprint(inst, "bandage", blueprintschance["bee_bandage"]) -- ������� ����
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("beebox", blueprintschance["bee_beebox"], 1)})
        table.insert(tooltip, {GetLootTooltip("bandage", blueprintschance["bee_bandage"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �����-������
PrefabSettingLootBlueprints("killerbee",
    function()
        blueprintschance["killerbee_bugnet"] = 2
        blueprintschance["killerbee_beeswax"] = 2
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "bugnet", blueprintschance["killerbee_bugnet"]) -- �����
        else
            AddLootBlueprint(inst, "beeswax", blueprintschance["killerbee_beeswax"]) -- �������� ����
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("bugnet", blueprintschance["killerbee_bugnet"], 1)})
        table.insert(tooltip, {GetLootTooltip("beeswax", blueprintschance["killerbee_beeswax"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������� �����
PrefabSettingLootBlueprints("beequeen",
    function()
        blueprintschance["beequeen_amulet"] = 50
        blueprintschance["beequeen_blueprint"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["beequeen_amulet"]) then
                inst.components.lootdropper:SpawnLootPrefab("amulet_blueprint") -- ������������ ������
            end
            if math.random() <= NormalizeChance(blueprintschance["beequeen_blueprint"]) then
                for i = 1, 3 do
                    inst.components.lootdropper:SpawnLootPrefab("blueprint")
                end
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("amulet", blueprintschance["beequeen_amulet"], 1),
                               GetLootTooltip("blueprint", blueprintschance["beequeen_blueprint"], 3)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���� / ����-�������� / ����-���������
local function PigmanChance()
    blueprintschance["pigman_hambat"] = 5
    blueprintschance["pigman_piggyback"] = 5
    blueprintschance["pigman_pighouse"] = 10
    blueprintschance["pigman_footballhat"] = 5
end

local function PigmanForceDropLoot(inst)
    if inst.components.lootdropper ~= nil then
        if inst:HasTag("werepig") then
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["pigman_hambat"]) then
                    inst.components.lootdropper:SpawnLootPrefab("hambat_blueprint") -- ������ ����
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["pigman_piggyback"]) then
                    inst.components.lootdropper:SpawnLootPrefab("piggyback_blueprint") -- ������ �����
                end
            end
        else
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["pigman_pighouse"]) then
                    inst.components.lootdropper:SpawnLootPrefab("pighouse_blueprint") -- ��� �����
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["pigman_footballhat"]) then
                    inst.components.lootdropper:SpawnLootPrefab("footballhat_blueprint") -- ���������� ����
                end
            end
        end
    end
end

local function PigmanTooltip(inst)
    local tooltip = {}
    if inst:HasTag("werepig") then
        table.insert(tooltip, {GetLootTooltip("hambat", blueprintschance["pigman_hambat"], 1)})
        table.insert(tooltip, {GetLootTooltip("piggyback", blueprintschance["pigman_piggyback"], 1)})
    else
        table.insert(tooltip, {GetLootTooltip("pighouse", blueprintschance["pigman_pighouse"], 1)})
        table.insert(tooltip, {GetLootTooltip("footballhat", blueprintschance["pigman_footballhat"], 1)})
    end
    return tooltip
end

PrefabSettingLootBlueprints("pigman", PigmanChance, nil, PigmanForceDropLoot, PigmanTooltip)
PrefabSettingLootBlueprints("pigguard", PigmanChance, nil, PigmanForceDropLoot, PigmanTooltip)
PrefabSettingLootBlueprints("moonpig", PigmanChance, nil, PigmanForceDropLoot, PigmanTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ����� ������
PrefabSettingLootBlueprints("icehound",
    function()
        blueprintschance["icehound_icestaff"] = 5
        blueprintschance["icehound_blueamulet"] = 5
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "icestaff", blueprintschance["icehound_icestaff"]) -- ������� �����
        else
            AddLootBlueprint(inst, "blueamulet", blueprintschance["icehound_blueamulet"]) -- ������� ������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("icestaff", blueprintschance["icehound_icestaff"], 1)})
        table.insert(tooltip, {GetLootTooltip("blueamulet", blueprintschance["icehound_blueamulet"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����������
PrefabSettingLootBlueprints("slurtle",
    function()
        blueprintschance["slurtle_armorwood"] = 5
    end,
    function(inst)
        AddLootBlueprint(inst, "armorwood", blueprintschance["slurtle_armorwood"]) -- ���������� �����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("armorwood", blueprintschance["slurtle_armorwood"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������
PrefabSettingLootBlueprints("spat",
    function()
        blueprintschance["spat_bearger_fur"] = 25
        blueprintschance["spat_cartographydesk"] = 25
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "bearger_fur", blueprintschance["spat_bearger_fur"]) -- ������� �����
        else
            AddLootBlueprint(inst, "cartographydesk", blueprintschance["spat_cartographydesk"]) -- ���� ����������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("bearger_fur", blueprintschance["spat_bearger_fur"], 1)})
        table.insert(tooltip, {GetLootTooltip("cartographydesk", blueprintschance["spat_cartographydesk"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������� �����
PrefabSettingLootBlueprints("robin_winter",
    function()
        blueprintschance["robin_winter_blowdart_pipe"] = 10
        blueprintschance["robin_winter_winterhat"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "blowdart_pipe", blueprintschance["robin_winter_blowdart_pipe"]) -- ������� ������
        else
            AddLootBlueprint(inst, "winterhat", blueprintschance["robin_winter_winterhat"]) -- ������ �����
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("blowdart_pipe", blueprintschance["robin_winter_blowdart_pipe"], 1)})
        table.insert(tooltip, {GetLootTooltip("winterhat", blueprintschance["robin_winter_winterhat"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������� (�������) / ���������� (���������)
local function BeakChance()
    blueprintschance["beak_nightsword"] = 5
    blueprintschance["beak_armor_sanity"] = 5
end

local function BeakLoot(inst)
    if math.random() < 0.5 then
        AddLootBlueprint(inst, "nightsword", blueprintschance["beak_nightsword"]) -- Ҹ���� ���
    else
        AddLootBlueprint(inst, "armor_sanity", blueprintschance["beak_armor_sanity"]) -- ����� ����
    end
end

local function BeakTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("nightsword", blueprintschance["beak_nightsword"], 1)})
    table.insert(tooltip, {GetLootTooltip("armor_sanity", blueprintschance["beak_armor_sanity"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("terrorbeak", BeakChance, BeakLoot, nil, BeakTooltip)
PrefabSettingLootBlueprints("nightmarebeak", BeakChance, BeakLoot, nil, BeakTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ������� ���� / ������� ����� / ������� ����
local function ShadowChesspiecesChance()
    blueprintschance["shadowchesspieces_moondial"] = 100
    blueprintschance["shadowchesspieces_panflute"] = 100
end
    
local function ShadowChesspiecesForceDropLoot(inst)
    if inst.level == 3 then
        if inst.components.lootdropper ~= nil then
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["shadowchesspieces_moondial"]) then
                    inst.components.lootdropper:SpawnLootPrefab("moondial_blueprint") -- ������ ����
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["shadowchesspieces_panflute"]) then
                    inst.components.lootdropper:SpawnLootPrefab("panflute_blueprint") -- ������ ����
                end
            end
        end
    end
end
    
local function ShadowChesspiecesTooltip(inst)
    local tooltip = {}
    if inst.level == 3 then
        table.insert(tooltip, {GetLootTooltip("moondial", blueprintschance["shadowchesspieces_moondial"], 1)})
        table.insert(tooltip, {GetLootTooltip("panflute", blueprintschance["shadowchesspieces_panflute"], 1)})
    end
    return tooltip
end

PrefabSettingLootBlueprints("shadow_knight", ShadowChesspiecesChance, nil, ShadowChesspiecesForceDropLoot, ShadowChesspiecesTooltip)
PrefabSettingLootBlueprints("shadow_rook", ShadowChesspiecesChance, nil, ShadowChesspiecesForceDropLoot, ShadowChesspiecesTooltip)
PrefabSettingLootBlueprints("shadow_bishop", ShadowChesspiecesChance, nil, ShadowChesspiecesForceDropLoot, ShadowChesspiecesTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("snurtle",
    function()
        blueprintschance["snurtle_armorwood"] = 5
    end,
    function(inst)
        AddLootBlueprint(inst, "armorwood", blueprintschance["snurtle_armorwood"]) -- ���������� �����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("armorwood", blueprintschance["snurtle_armorwood"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������-�����
PrefabSettingLootBlueprints("deerclops",
    function()
        blueprintschance["deerclops_eyebrellahat"] = 100
        blueprintschance["deerclops_blueprints"] = 100
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["deerclops_eyebrellahat"]) then            
                inst.components.lootdropper:SpawnLootPrefab("eyebrellahat_blueprint") -- ���������
            end
            if math.random() <= NormalizeChance(blueprintschance["deerclops_blueprints"]) then
                for i = 1, 3 do
                    inst.components.lootdropper:SpawnLootPrefab("blueprint")
                end
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("eyebrellahat", blueprintschance["deerclops_eyebrellahat"], 1),
                               GetLootTooltip("blueprint", blueprintschance["deerclops_blueprints"], 3)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������
PrefabSettingLootBlueprints("tentacle",
    function()
        blueprintschance["tentacle_spear"] = 10
        blueprintschance["tentacle_beemine"] = 10
    end,
    function(inst)
        if math.random() < 0.5 then
            AddLootBlueprint(inst, "spear", blueprintschance["tentacle_spear"]) -- �����
        else
            AddLootBlueprint(inst, "beemine", blueprintschance["tentacle_beemine"]) -- ���������
        end
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("spear", blueprintschance["tentacle_spear"], 1)})
        table.insert(tooltip, {GetLootTooltip("beemine", blueprintschance["tentacle_beemine"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����
local function LeifChance()
    blueprintschance["leif_treasurechest"] = 10
    blueprintschance["leif_boat_item"] = 5
    blueprintschance["leif_oar"] = 5
end

local function LeifLoot(inst)
    local rnd = math.random()
    if rnd >= 0 and rnd <= 0.33 then
        AddLootBlueprint(inst, "treasurechest", blueprintschance["leif_treasurechest"]) -- ������
    elseif rnd > 0.33 and rnd <= 0.66 then
        AddLootBlueprint(inst, "boat_item", blueprintschance["leif_boat_item"]) -- ����� ��� �����
    else
        AddLootBlueprint(inst, "oar", blueprintschance["leif_oar"]) -- �����    
    end
end

local function LeifTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("treasurechest", blueprintschance["leif_treasurechest"], 1)})
    table.insert(tooltip, {GetLootTooltip("boat_item", blueprintschance["leif_boat_item"], 1)})
    table.insert(tooltip, {GetLootTooltip("oar", blueprintschance["leif_oar"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("leif", LeifChance, LeifLoot, nil, LeifTooltip)
PrefabSettingLootBlueprints("leif_sparse", LeifChance, LeifLoot, nil, LeifTooltip)
-- ==========================================================================================================
--
--
-- Plants and structures
--
--
-- ==========================================================================================================
-- ����� ������������
PrefabSettingLootBlueprints("rock2",
    function()
        blueprintschance["rock2_goldenpickaxe"] = 5
    end,
    nil,
    function(inst)
        if math.random() <= NormalizeChance(blueprintschance["rock2_goldenpickaxe"]) then
            if inst.components.lootdropper ~= nil then
            inst.components.lootdropper:SpawnLootPrefab("goldenpickaxe_blueprint") -- ������� �����
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("goldenpickaxe", blueprintschance["rock2_goldenpickaxe"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����� ������
local function RockMoonChance()
    blueprintschance["rock_moon_moonrockcrater"] = 10
    blueprintschance["rock_moon_wall_moonrock_item"] = 10
end

local function RockMoonForceDropLoot(inst)
    if inst.components.lootdropper ~= nil then
        if math.random() < 0.5 then
            if math.random() <= NormalizeChance(blueprintschance["rock_moon_moonrockcrater"]) then
                inst.components.lootdropper:SpawnLootPrefab("moonrockcrater_blueprint") -- ������ ������ � �������
            end
        else
            if math.random() <= NormalizeChance(blueprintschance["rock_moon_wall_moonrock_item"]) then
                inst.components.lootdropper:SpawnLootPrefab("wall_moonrock_item_blueprint") -- ����� �� ������� �����
            end
        end
    end
end

local function RockMoonTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("moonrockcrater", blueprintschance["rock_moon_moonrockcrater"], 1)})
    table.insert(tooltip, {GetLootTooltip("wall_moonrock_item", blueprintschance["rock_moon_wall_moonrock_item"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("rock_moon", RockMoonChance, nil, RockMoonForceDropLoot, RockMoonTooltip)
PrefabSettingLootBlueprints("rock_moon_shell", RockMoonChance, nil, RockMoonForceDropLoot, RockMoonTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ����� �������
PrefabSettingLootBlueprints("rock1",
    function()
        blueprintschance["rock1_coldfire"] = 2.5
        blueprintschance["rock1_saltlick"] = 2.5
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["rock1_coldfire"]) then
                    inst.components.lootdropper:SpawnLootPrefab("coldfire_blueprint") -- ��������������� ������
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["rock1_saltlick"]) then
                    inst.components.lootdropper:SpawnLootPrefab("saltlick_blueprint") -- ������� �������
                end
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("coldfire", blueprintschance["rock1_coldfire"], 1)})
        table.insert(tooltip, {GetLootTooltip("saltlick", blueprintschance["rock1_saltlick"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ����� ������
local function RockFlintlessChance()
    blueprintschance["rock_flintless_wall_stone_item"] = 2.5
end

local function RockFlintlessTallForceDropLoot(inst)
    if inst.components.lootdropper ~= nil then
        if math.random() <= NormalizeChance(blueprintschance["rock_flintless_wall_stone_item"]) then
            inst.components.lootdropper:SpawnLootPrefab("wall_stone_item_blueprint") -- �������� �����
        end
    end
end

local function RockFlintlessTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("wall_stone_item", blueprintschance["rock_flintless_wall_stone_item"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("rock_flintless", RockFlintlessChance, nil, RockFlintlessTallForceDropLoot, RockFlintlessTooltip)
PrefabSettingLootBlueprints("rock_flintless_med", RockFlintlessChance, nil, RockFlintlessTallForceDropLoot, RockFlintlessTooltip)
PrefabSettingLootBlueprints("rock_flintless_low", RockFlintlessChance, nil, RockFlintlessTallForceDropLoot, RockFlintlessTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ��������� ������
local function TwiggyTreeChance()
    blueprintschance["twiggytree_shovel"] = 2.5
end

local function TwiggyTreeTallLoot(inst)
    inst:ListenForEvent("workfinished", function(inst, data)
        if inst.components.lootdropper ~= nil then
            if math.random() <= NormalizeChance(blueprintschance["twiggytree_shovel"]) then
                inst.components.lootdropper:SpawnLootPrefab("shovel_blueprint") -- ������
            end
        end
    end)
end

local function TwiggyTreeTooltip(inst)
    local tooltip = {}
    if not inst:HasTag("stump") then
        table.insert(tooltip, {GetLootTooltip("shovel", blueprintschance["twiggytree_shovel"], 1)})
    end
    return tooltip
end

PrefabSettingLootBlueprints("twiggytree", TwiggyTreeChance, TwiggyTreeTallLoot, nil, TwiggyTreeTooltip)
PrefabSettingLootBlueprints("twiggy_normal", TwiggyTreeChance, TwiggyTreeTallLoot, nil, TwiggyTreeTooltip)
PrefabSettingLootBlueprints("twiggy_tall", TwiggyTreeChance, TwiggyTreeTallLoot, nil, TwiggyTreeTooltip)
PrefabSettingLootBlueprints("twiggy_short", TwiggyTreeChance, TwiggyTreeTallLoot, nil, TwiggyTreeTooltip)
PrefabSettingLootBlueprints("twiggy_old", TwiggyTreeChance, TwiggyTreeTallLoot, nil, TwiggyTreeTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ������� ���������
local function StalagmiteTallChance()
    blueprintschance["stalagmite_tall_turf_road"] = 2.5
end

local function StalagmiteTallForceDropLoot(inst)
    if inst.components.lootdropper ~= nil then
        if math.random() <= NormalizeChance(blueprintschance["stalagmite_tall_turf_road"]) then
            inst.components.lootdropper:SpawnLootPrefab("turf_road_blueprint") -- ���������
        end
    end
end

local function StalagmiteTallTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("turf_road", blueprintschance["stalagmite_tall_turf_road"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("stalagmite_tall", StalagmiteTallChance, nil, StalagmiteTallForceDropLoot, StalagmiteTallTooltip)
PrefabSettingLootBlueprints("stalagmite_tall_full", StalagmiteTallChance, nil, StalagmiteTallForceDropLoot, StalagmiteTallTooltip)
PrefabSettingLootBlueprints("stalagmite_tall_med", StalagmiteTallChance, nil, StalagmiteTallForceDropLoot, StalagmiteTallTooltip)
PrefabSettingLootBlueprints("stalagmite_tall_low", StalagmiteTallChance, nil, StalagmiteTallForceDropLoot, StalagmiteTallTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ������� �������
local function MushTreeChance()
    blueprintschance["mushtree_goldenaxe"] = 5
end

local function MushTreeForceDropLoot(inst)
    if math.random() <= NormalizeChance(blueprintschance["mushtree_goldenaxe"]) then
        if inst.components.lootdropper ~= nil then
            inst.components.lootdropper:SpawnLootPrefab("goldenaxe_blueprint") -- ��������� �����
        end
    end
end

local function MushTreeTooltip(inst)
    local tooltip = {}
    if not inst:HasTag("stump") then
        table.insert(tooltip, {GetLootTooltip("goldenaxe", blueprintschance["mushtree_goldenaxe"], 1)})
    end
    return tooltip
end

PrefabSettingLootBlueprints("mushtree_tall", MushTreeChance, nil, MushTreeForceDropLoot, MushTreeTooltip)
PrefabSettingLootBlueprints("mushtree_medium", MushTreeChance, nil, MushTreeForceDropLoot, MushTreeTooltip)
PrefabSettingLootBlueprints("mushtree_small", MushTreeChance, nil, MushTreeForceDropLoot, MushTreeTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- �����
local function MushroomsChance()
    blueprintschance["mushrooms_mushroom_farm"] = 2
end

local function MushroomsLoot(inst)
    inst:ListenForEvent("picked", function(inst, data)     
        if math.random() <= NormalizeChance(blueprintschance["mushrooms_mushroom_farm"]) then
            if inst.components.lootdropper ~= nil then
                inst.components.lootdropper:SpawnLootPrefab("mushroom_farm_blueprint") -- ������� ���������
            end
        end
    end)
end

local function MushroomsTooltip(inst)
    local tooltip = {}
    if inst:HasTag("pickable") then
        table.insert(tooltip, {GetLootTooltip("mushroom_farm", blueprintschance["mushrooms_mushroom_farm"], 1)})
    end
    return tooltip
end

PrefabSettingLootBlueprints("red_mushroom", MushroomsChance, MushroomsLoot, nil, MushroomsTooltip)
PrefabSettingLootBlueprints("green_mushroom", MushroomsChance, MushroomsLoot, nil, MushroomsTooltip)
PrefabSettingLootBlueprints("blue_mushroom", MushroomsChance, MushroomsLoot, nil, MushroomsTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ��� / ��������� ���
local function EvergreenChance()
    blueprintschance["evergreen_boards"] = 1
    blueprintschance["evergreen_turf_woodfloor"] = 1
end

local function EvergreenLoot(inst)
    inst:ListenForEvent("workfinished", function(inst, data)
        if inst.components.lootdropper ~= nil then
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["evergreen_boards"]) then
                    inst.components.lootdropper:SpawnLootPrefab("boards_blueprint") -- �����
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["evergreen_turf_woodfloor"]) then
                    inst.components.lootdropper:SpawnLootPrefab("turf_woodfloor_blueprint") -- ������
                end
            end
        end
    end)
end

local function EvergreenTooltip(inst)
    local tooltip = {}
    if not inst:HasTag("stump") then
        table.insert(tooltip, {GetLootTooltip("boards", blueprintschance["evergreen_boards"], 1)})
        table.insert(tooltip, {GetLootTooltip("turf_woodfloor", blueprintschance["evergreen_turf_woodfloor"], 1)})
    end
    return tooltip
end

PrefabSettingLootBlueprints("evergreen", EvergreenChance, EvergreenLoot, nil, EvergreenTooltip)
PrefabSettingLootBlueprints("evergreen_normal", EvergreenChance, EvergreenLoot, nil, EvergreenTooltip)
PrefabSettingLootBlueprints("evergreen_tall", EvergreenChance, EvergreenLoot, nil, EvergreenTooltip)
PrefabSettingLootBlueprints("evergreen_short", EvergreenChance, EvergreenLoot, nil, EvergreenTooltip)
PrefabSettingLootBlueprints("evergreen_sparse", EvergreenChance, EvergreenLoot, nil, EvergreenTooltip)
PrefabSettingLootBlueprints("evergreen_sparse_normal", EvergreenChance, EvergreenLoot, nil, EvergreenTooltip)
PrefabSettingLootBlueprints("evergreen_sparse_tall", EvergreenChance, EvergreenLoot, nil, EvergreenTooltip)
PrefabSettingLootBlueprints("evergreen_sparse_short", EvergreenChance, EvergreenLoot, nil, EvergreenTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ������
local function CactusChance()
    blueprintschance["cactus_reflectivevest"] = 5
    blueprintschance["cactus_siestahut"] = 5
end

local function CactusLoot(inst)
    inst:ListenForEvent("picked", function(inst, data)
        if inst.components.lootdropper ~= nil then
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["cactus_reflectivevest"]) then
                    inst.components.lootdropper:SpawnLootPrefab("reflectivevest_blueprint") -- ������ ��������
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["cactus_siestahut"]) then
                    inst.components.lootdropper:SpawnLootPrefab("siestahut_blueprint") -- ����� ��� ������
                end
            end
        end
    end)
end

local function CactusTooltip(inst)
    local tooltip = {}
    if inst:HasTag("pickable") then
        table.insert(tooltip, {GetLootTooltip("reflectivevest", blueprintschance["cactus_reflectivevest"], 1)})
        table.insert(tooltip, {GetLootTooltip("siestahut", blueprintschance["cactus_siestahut"], 1)})
    end
    return tooltip
end

PrefabSettingLootBlueprints("cactus", CactusChance, CactusLoot, nil, CactusTooltip)
PrefabSettingLootBlueprints("oasis_cactus", CactusChance, CactusLoot, nil, CactusTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- �����
PrefabSettingLootBlueprints("reeds",
    function()
        blueprintschance["reeds_papyrus"] = 2.5
        blueprintschance["reeds_wall_hay_item"] = 2.5
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)
            if inst.components.lootdropper ~= nil then
                if math.random() < 0.5 then
                    if math.random() <= NormalizeChance(blueprintschance["reeds_papyrus"]) then
                        inst.components.lootdropper:SpawnLootPrefab("papyrus_blueprint") -- �������
                    end
                else
                    if math.random() <= NormalizeChance(blueprintschance["reeds_wall_hay_item"]) then
                        inst.components.lootdropper:SpawnLootPrefab("wall_hay_item_blueprint") -- ����� �� �����
                    end
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("papyrus", blueprintschance["reeds_papyrus"], 1)})
            table.insert(tooltip, {GetLootTooltip("wall_hay_item", blueprintschance["reeds_wall_hay_item"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������� ����
PrefabSettingLootBlueprints("marsh_bush",
    function()
        blueprintschance["marsh_bush_meatrack"] = 5
        blueprintschance["marsh_bush_fence_item"] = 5
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)                    
            if inst.components.lootdropper ~= nil then
                if math.random() < 0.5 then
                    if math.random() <= NormalizeChance(blueprintschance["marsh_bush_meatrack"]) then
                        inst.components.lootdropper:SpawnLootPrefab("meatrack_blueprint") -- �������
                    end
                else
                    if math.random() <= NormalizeChance(blueprintschance["marsh_bush_fence_item"]) then
                        inst.components.lootdropper:SpawnLootPrefab("fence_item_blueprint") -- ���������� �����
                    end
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("meatrack", blueprintschance["marsh_bush_meatrack"], 1)})
            table.insert(tooltip, {GetLootTooltip("fence_item", blueprintschance["marsh_bush_fence_item"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������� ������
local function MarbletreeChance()
    blueprintschance["marbletree_sculptingtable"] = 5
    blueprintschance["marbletree_armormarble"] = 5
end

local function MarbletreeForceDropLoot(inst)
    if inst.components.lootdropper ~= nil then
        if math.random() < 0.5 then
            if math.random() <= NormalizeChance(blueprintschance["marbletree_sculptingtable"]) then
                inst.components.lootdropper:SpawnLootPrefab("sculptingtable_blueprint") -- ��������� ����
            end
        else
            if math.random() <= NormalizeChance(blueprintschance["marbletree_armormarble"]) then
                inst.components.lootdropper:SpawnLootPrefab("armormarble_blueprint") -- ��������� �����
            end
        end
    end
end

local function MarbletreeTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("sculptingtable", blueprintschance["marbletree_sculptingtable"], 1)})
    table.insert(tooltip, {GetLootTooltip("armormarble", blueprintschance["marbletree_armormarble"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("marbletree", MarbletreeChance, nil, MarbletreeForceDropLoot, MarbletreeTooltip)
PrefabSettingLootBlueprints("marbletree_1", MarbletreeChance, nil, MarbletreeForceDropLoot, MarbletreeTooltip)
PrefabSettingLootBlueprints("marbletree_2", MarbletreeChance, nil, MarbletreeForceDropLoot, MarbletreeTooltip)
PrefabSettingLootBlueprints("marbletree_3", MarbletreeChance, nil, MarbletreeForceDropLoot, MarbletreeTooltip)
PrefabSettingLootBlueprints("marbletree_4", MarbletreeChance, nil, MarbletreeForceDropLoot, MarbletreeTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ���������� ������
local function RockPetrifiedTreeChance()
    blueprintschance["rock_petrified_tree_goldenshovel"] = 5
    blueprintschance["rock_petrified_tree_homesign"] = 5
end

local function RockPetrifiedTreeForceDropLoot(inst)
    if inst.components.lootdropper ~= nil then
        if math.random() < 0.5 then
            if math.random() <= NormalizeChance(blueprintschance["rock_petrified_tree_goldenshovel"]) then
                inst.components.lootdropper:SpawnLootPrefab("goldenshovel_blueprint") -- ����������� ������
            end
        else
            if math.random() <= NormalizeChance(blueprintschance["rock_petrified_tree_homesign"]) then
                inst.components.lootdropper:SpawnLootPrefab("homesign_blueprint") -- ��������
            end
        end
    end
end

local function RockPetrifiedTreeTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("goldenshovel", blueprintschance["rock_petrified_tree_goldenshovel"], 1)})
    table.insert(tooltip, {GetLootTooltip("homesign", blueprintschance["rock_petrified_tree_homesign"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("rock_petrified_tree", RockPetrifiedTreeChance, nil, RockPetrifiedTreeForceDropLoot, RockPetrifiedTreeTooltip)
PrefabSettingLootBlueprints("rock_petrified_tree_med", RockPetrifiedTreeChance, nil, RockPetrifiedTreeForceDropLoot, RockPetrifiedTreeTooltip)
PrefabSettingLootBlueprints("rock_petrified_tree_tall", RockPetrifiedTreeChance, nil, RockPetrifiedTreeForceDropLoot, RockPetrifiedTreeTooltip)
PrefabSettingLootBlueprints("rock_petrified_tree_short", RockPetrifiedTreeChance, nil, RockPetrifiedTreeForceDropLoot, RockPetrifiedTreeTooltip)
PrefabSettingLootBlueprints("rock_petrified_tree_old", RockPetrifiedTreeChance, nil, RockPetrifiedTreeForceDropLoot, RockPetrifiedTreeTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ����������
local function DropLootCaveFern(inst)
    if math.random() <= NormalizeChance(blueprintschance["cave_fern_pottedfern"]) then
        if inst.components.lootdropper ~= nil then
            inst.components.lootdropper:SpawnLootPrefab("pottedfern_blueprint") -- ���������� � ������
        end
    end
end

local function OnCaveFernPickedFn(inst, picker)
    DropLootCaveFern(inst)
    inst:Remove()
end

PrefabSettingLootBlueprints("cave_fern",
    function()
        blueprintschance["cave_fern_pottedfern"] = 5
    end,
    function(inst)
        inst:AddComponent("lootdropper")
        inst.components.pickable.onpickedfn = OnCaveFernPickedFn 
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("pottedfern", blueprintschance["cave_fern_pottedfern"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������� ����
local function TumbleweedMakeLoot(inst)
    local possible_loot =
    {
        {chance = 40,   item = "cutgrass"},
        {chance = 40,   item = "twigs"},
        {chance = 1,    item = "petals"},
        {chance = 1,    item = "foliage"},
        {chance = 1,    item = "silk"},
        {chance = 1,    item = "rope"},
        {chance = 2,    item = "seeds"},
        {chance = 0.01, item = "purplegem"},
        {chance = 0.04, item = "bluegem"},
        {chance = 0.02, item = "redgem"},
        {chance = 0.02, item = "orangegem"},
        {chance = 0.01, item = "yellowgem"},
        {chance = 0.02, item = "greengem"},
        {chance = 0.5,  item = "trinket_6"},
        {chance = 0.5,  item = "trinket_4"},
        {chance = 1,    item = "cutreeds"},
        {chance = 0.33, item = "feather_crow"},
        {chance = 0.33, item = "feather_robin"},
        {chance = 0.33, item = "feather_robin_winter"},
        {chance = 0.33, item = "feather_canary"},
        {chance = 1,    item = "trinket_3"},
        {chance = 1,    item = "beefalowool"},
        {chance = 0.1,  item = "rabbit"},
        {chance = 0.1,  item = "mole"},
        {chance = 0.1,  item = "spider", aggro = true},
        {chance = 0.1,  item = "frog", aggro = true},
        {chance = 0.1,  item = "bee", aggro = true},
        {chance = 0.1,  item = "mosquito", aggro = true},
        {chance = 1,    item = "butterflywings"},
        {chance = .02,  item = "beardhair"},
        {chance = 1,    item = "berries"},
        {chance = 1,    item = "petals_evil"},
        {chance = 1,    item = "trinket_8"},
        {chance = 1,    item = "houndstooth"},
        {chance = 1,    item = "stinger"},
        {chance = 1,    item = "gears"},
        {chance = 0.1,  item = "boneshard"},
    }

    local chessunlocks = TheWorld.components.chessunlocks
    if chessunlocks ~= nil then
        local CHESS_LOOT = {
            "chesspiece_pawn_sketch",
            "chesspiece_muse_sketch",
            "chesspiece_formal_sketch",
            "trinket_15", --bishop
            "trinket_16", --bishop
            "trinket_28", --rook
            "trinket_29", --rook
            "trinket_30", --knight
            "trinket_31", --knight
        }
        for i, v in ipairs(CHESS_LOOT) do
            if not chessunlocks:IsLocked(v) then
                table.insert(possible_loot, { chance = .1, item = v })
            end
        end
    end

    local totalchance = 0
    for m, n in ipairs(possible_loot) do
        totalchance = totalchance + n.chance
    end

    inst.loot = {}
    inst.lootaggro = {}
    local next_loot = nil
    local next_aggro = nil
    local next_chance = nil
    local num_loots = 3
    while num_loots > 0 do
        next_chance = math.random()*totalchance
        next_loot = nil
        next_aggro = nil
        for m, n in ipairs(possible_loot) do
            next_chance = next_chance - n.chance
            if next_chance <= 0 then
                next_loot = n.item
                if n.aggro then next_aggro = true end
                break
            end
        end
        if next_loot ~= nil then
            table.insert(inst.loot, next_loot)
            if next_aggro then 
                table.insert(inst.lootaggro, true)
            else
                table.insert(inst.lootaggro, false)
            end
            num_loots = num_loots - 1
        end
    end
end

PrefabSettingLootBlueprints("tumbleweed",
    nil,
    function(inst)
        TumbleweedMakeLoot(inst)
    end,
    nil,
    nil)
-- ----------------------------------------------------------------------------------------------------------
-- �������� ��������� ������
PrefabSettingLootBlueprints("cave_banana_tree",
    function()
        blueprintschance["cave_banana_tree_hawaiianshirt"] = 2.5
        blueprintschance["cave_banana_tree_treasurechest"] = 2.5
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)                    
            if inst.components.lootdropper ~= nil then
                if math.random() < 0.5 then
                    if math.random() <= NormalizeChance(blueprintschance["cave_banana_tree_hawaiianshirt"]) then
                        inst.components.lootdropper:SpawnLootPrefab("hawaiianshirt_blueprint") -- ��������� �������
                    end
                else
                    if math.random() <= NormalizeChance(blueprintschance["cave_banana_tree_treasurechest"]) then
                        inst.components.lootdropper:SpawnLootPrefab("treasurechest_blueprint") -- ������
                    end
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("hawaiianshirt", blueprintschance["cave_banana_tree_hawaiianshirt"], 1)})
            table.insert(tooltip, {GetLootTooltip("treasurechest", blueprintschance["cave_banana_tree_treasurechest"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������� ��������
PrefabSettingLootBlueprints("lichen",
    function()
        blueprintschance["lichen_fertilizer"] = 1
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)                    
            if math.random() <= NormalizeChance(blueprintschance["lichen_fertilizer"]) then
                if inst.components.lootdropper ~= nil then
                    inst.components.lootdropper:SpawnLootPrefab("fertilizer_blueprint") -- ����� � �����������
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("fertilizer", blueprintschance["lichen_fertilizer"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("sapling",
    function()
        blueprintschance["sapling_shovel"] = 1
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)                    
            if math.random() <= NormalizeChance(blueprintschance["sapling_shovel"]) then
                if inst.components.lootdropper ~= nil then
                    inst.components.lootdropper:SpawnLootPrefab("shovel_blueprint") -- ������
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("shovel", blueprintschance["sapling_shovel"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������
local function StalagmiteChance()
    blueprintschance["stalagmite_cutstone"] = 5
end

local function StalagmiteForceDropLoot(inst)
    if inst.components.lootdropper ~= nil then
        if math.random() <= NormalizeChance(blueprintschance["stalagmite_cutstone"]) then
            inst.components.lootdropper:SpawnLootPrefab("cutstone_blueprint") -- �������� ����
        end
    end
end

local function StalagmiteTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("cutstone", blueprintschance["stalagmite_cutstone"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("stalagmite", StalagmiteChance, nil, StalagmiteForceDropLoot, StalagmiteTooltip)
PrefabSettingLootBlueprints("stalagmite_full", StalagmiteChance, nil, StalagmiteForceDropLoot, StalagmiteTooltip)
PrefabSettingLootBlueprints("stalagmite_med", StalagmiteChance, nil, StalagmiteForceDropLoot, StalagmiteTooltip)
PrefabSettingLootBlueprints("stalagmite_low", StalagmiteChance, nil, StalagmiteForceDropLoot, StalagmiteTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ����� ���������� ����� (������������ ��������)
PrefabSettingLootBlueprints("wormlight_plant",
    function()
        blueprintschance["wormlight_plant_sentryward"] = 5
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)                    
            if math.random() <= NormalizeChance(blueprintschance["wormlight_plant_sentryward"]) then
                if inst.components.lootdropper ~= nil then
                    inst.components.lootdropper:SpawnLootPrefab("sentryward_blueprint") -- ���������
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("sentryward", blueprintschance["wormlight_plant_sentryward"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- �����
PrefabSettingLootBlueprints("grass",
    function()
        blueprintschance["grass_rope"] = 1
        blueprintschance["grass_wall_wood_item"] = 1
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["grass_rope"]) then
                    inst.components.lootdropper:SpawnLootPrefab("rope_blueprint") -- �������
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["grass_wall_wood_item"]) then
                    inst.components.lootdropper:SpawnLootPrefab("wall_wood_item_blueprint") -- ���������� �����
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("rope", blueprintschance["grass_rope"], 1)})
            table.insert(tooltip, {GetLootTooltip("wall_wood_item", blueprintschance["grass_wall_wood_item"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ������ / ���� / ������� ������
local function DropLootFlower(inst)
    if inst.components.lootdropper ~= nil then
        if math.random() < 0.5 then
            if math.random() <= NormalizeChance(blueprintschance["flower_rainometer"]) then
                inst.components.lootdropper:SpawnLootPrefab("rainometer_blueprint") -- ���������
            end
        else
            if math.random() <= NormalizeChance(blueprintschance["flower_winterometer"]) then
                inst.components.lootdropper:SpawnLootPrefab("winterometer_blueprint") -- ���������� �����������
            end
        end
    end
end

local function OnFlowerPickedFn(inst, picker)
    if picker and picker.components.sanity then
        picker.components.sanity:DoDelta(TUNING.SANITY_TINY)
    end

    if inst.animname == "rose" and picker.components.combat ~= nil then
        picker.components.combat:GetAttacked(inst, TUNING.ROSE_DAMAGE)
        picker:PushEvent("thorns")
    end

	if not inst.planted then
		TheWorld:PushEvent("beginregrowth", inst)
	end

    DropLootFlower(inst)

    inst:Remove()
end

local function FlowerChance()
    blueprintschance["flower_rainometer"] = 2
    blueprintschance["flower_winterometer"] = 2
end

local function FlowerLoot(inst)
    if inst.components.lootdropper == nil then
        inst:AddComponent("lootdropper")
    end    
    inst.components.pickable.onpickedfn = OnFlowerPickedFn  
end

local function FlowerTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("rainometer", blueprintschance["flower_rainometer"], 1)})
    table.insert(tooltip, {GetLootTooltip("winterometer", blueprintschance["flower_winterometer"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("flower", FlowerChance, FlowerLoot, nil, FlowerTooltip)
PrefabSettingLootBlueprints("flower_rose", FlowerChance, FlowerLoot, nil, FlowerTooltip)
PrefabSettingLootBlueprints("flower_evil", FlowerChance, FlowerLoot, nil, FlowerTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ������ �����
local function FlowerCaveChance()
    blueprintschance["flower_cave_lantern"] = 1
end

local function FlowerCaveLoot(inst)
    inst:ListenForEvent("picked", function(inst, data)        
        if math.random() <= NormalizeChance(blueprintschance["flower_cave_lantern"]) then
            if inst.components.lootdropper ~= nil then
                inst.components.lootdropper:SpawnLootPrefab("lantern_blueprint") -- ������
            end
        end    
    end)
end

local function FlowerCaveTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("lantern", blueprintschance["flower_cave_lantern"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("flower_cave", FlowerCaveChance, FlowerCaveLoot, nil, FlowerCaveTooltip)
PrefabSettingLootBlueprints("flower_cave_double", FlowerCaveChance, FlowerCaveLoot, nil, FlowerCaveTooltip)
PrefabSettingLootBlueprints("flower_cave_triple", FlowerCaveChance, FlowerCaveLoot, nil, FlowerCaveTooltip)
-- ----------------------------------------------------------------------------------------------------------
-- ������� ���� / ���� � ������� ������
local function BerrybushChance()
    blueprintschance["berrybush_bushhat"] = 2
end

local function BerrybushLoot(inst)
    inst:ListenForEvent("picked", function(inst, data)        
        if math.random() <= NormalizeChance(blueprintschance["berrybush_bushhat"]) then
            if inst.components.lootdropper ~= nil then
                inst.components.lootdropper:SpawnLootPrefab("bushhat_blueprint") -- ����������
            end
        end
    end)
end

local function BerrybushTooltip(inst)
    local tooltip = {}
    if inst:HasTag("pickable") then
        table.insert(tooltip, {GetLootTooltip("bushhat", blueprintschance["berrybush_bushhat"], 1)})
    end
    return tooltip
end

PrefabSettingLootBlueprints("berrybush", BerrybushChance, BerrybushLoot, nil, BerrybushTooltip)
PrefabSettingLootBlueprints("berrybush2", BerrybushChance, BerrybushLoot, nil, BerrybushTooltip)
PrefabSettingLootBlueprints("berrybush_juicy", BerrybushChance, BerrybushLoot, nil, BerrybushTooltip)

-- ----------------------------------------------------------------------------------------------------------
-- update: Return of them
-- ----------------------------------------------------------------------------------------------------------
-- ���� �������� �������
PrefabSettingLootBlueprints("rock_avocado_bush",
    function()
        blueprintschance["rock_avocado_bush_bugnet"] = 5
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)
			if math.random() <= NormalizeChance(blueprintschance["rock_avocado_bush_bugnet"]) then
				inst.components.lootdropper:SpawnLootPrefab("bugnet_blueprint") -- �����
			end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("bugnet", blueprintschance["rock_avocado_bush_bugnet"], 1)})
        end
        return tooltip
    end)

-- ----------------------------------------------------------------------------------------------------------
-- ������� ������
PrefabSettingLootBlueprints("sapling_moon",
    function()
        blueprintschance["sapling_goldenshovel"] = 10
    end,
    function(inst)
        inst:ListenForEvent("picked", function(inst, data)                    
            if math.random() <= NormalizeChance(blueprintschance["sapling_goldenshovel"]) then
                if inst.components.lootdropper ~= nil then
                    inst.components.lootdropper:SpawnLootPrefab("goldenshovel_blueprint") -- ����������� ������
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if inst:HasTag("pickable") then
            table.insert(tooltip, {GetLootTooltip("goldenshovel", blueprintschance["sapling_goldenshovel"], 1)})
        end
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ��������� ������
PrefabSettingLootBlueprints("moon_tree",
    function()
        blueprintschance["moon_tree_purplegem"] = 15
    end,
    function(inst)
        inst:ListenForEvent("workfinished", function(inst, data)
            if inst.components.lootdropper ~= nil then
                if math.random() <= NormalizeChance(blueprintschance["moon_tree_purplegem"]) then
                    inst.components.lootdropper:SpawnLootPrefab("purplegem_blueprint") -- ���������� ��������
                end
            end
        end)
    end,
    nil,
    function(inst)
        local tooltip = {}
        if not inst:HasTag("stump") then
            table.insert(tooltip, {GetLootTooltip("purplegem", blueprintschance["moon_tree_purplegem"], 1)})
        end
        return tooltip
    end)

-- ----------------------------------------------------------------------------------------------------------
-- �������� ����
PrefabSettingLootBlueprints("spider_moon",
    function()
        blueprintschance["spider_moon_mast_item"] = 2.5
    end,
    function(inst)
         AddLootBlueprint(inst, "mast_item", blueprintschance["spider_moon_mast_item"]) -- �����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("mast_item", blueprintschance["spider_moon_mast_item"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���������� �������
PrefabSettingLootBlueprints("cookiecutter",
    function()
        blueprintschance["cookiecutter_cookiecutterhat"] = 1
    end,
    function(inst)
         AddLootBlueprint(inst, "cookiecutterhat", blueprintschance["cookiecutter_cookiecutterhat"]) -- ��������� ����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("cookiecutterhat", blueprintschance["cookiecutter_cookiecutterhat"], 1)})
        return tooltip
    end)	
	
-- ----------------------------------------------------------------------------------------------------------
-- ������� �����������
PrefabSettingLootBlueprints("saltstack",
    function()
        blueprintschance["saltstack_saltbox"] = 1
    end,
    nil,
    function(inst)
        if math.random() <= NormalizeChance(blueprintschance["saltstack_saltbox"]) then
            if inst.components.lootdropper ~= nil then
            inst.components.lootdropper:SpawnLootPrefab("saltbox_blueprint") -- ������� ����
            end
        end
    end,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("saltbox", blueprintschance["saltstack_saltbox"], 1)})
        return tooltip
    end)
-- ----------------------------------------------------------------------------------------------------------
-- ���� (�����)
PrefabSettingLootBlueprints("pondfish",
    function()
        blueprintschance["pondfish_pocket_scale"] = 5
    end,
    function(inst)
		AddLootBlueprint(inst, "pocket_scale", blueprintschance["pondfish_pocket_scale"]) -- ��������� ����
    end,
    nil,
    function(inst)
        local tooltip = {}
        table.insert(tooltip, {GetLootTooltip("pocket_scale", blueprintschance["pondfish_pocket_scale"], 1)})
        return tooltip
    end)

-- ----------------------------------------------------------------------------------------------------------
-- �������
PrefabSettingLootBlueprints("squid",
    function()
        blueprintschance["squid_lantern"] = 1
        blueprintschance["squid_blowdart_pipe"] = 1
    end,
    nil,
    function(inst)
        if inst.components.lootdropper ~= nil then
            if math.random() < 0.5 then
                if math.random() <= NormalizeChance(blueprintschance["squid_lantern"]) then            
                    inst.components.lootdropper:SpawnLootPrefab("lantern_blueprint") -- ������
                end
            else
                if math.random() <= NormalizeChance(blueprintschance["squid_blowdart_pipe"]) then
                    inst.components.lootdropper:SpawnLootPrefab("blowdart_pipe_blueprint") -- ������� ������
                end
            end
        end
    end,
    function(inst)
        local tooltip = {}         
        table.insert(tooltip, {GetLootTooltip("lantern", blueprintschance["squid_lantern"], 1)})
        table.insert(tooltip, {GetLootTooltip("blowdart_pipe", blueprintschance["squid_blowdart_pipe"], 1)})
        return tooltip
    end)

-- ----------------------------------------------------------------------------------------------------------
-- ������������ ����
local function OceanfishChance()
    blueprintschance["oceanfish_trophyscale_fish"] = 2
    blueprintschance["oceanfish_fish_box"] = 2
end

local function OceanfishLoot(inst)
    if math.random() < 0.5 then
        AddLootBlueprint(inst, "trophyscale_fish", blueprintschance["oceanfish_trophyscale_fish"]) -- ����-��������
    else
        AddLootBlueprint(inst, "fish_box", blueprintschance["oceanfish_fish_box"]) -- �������� ������� ��� �������
    end
end

local function OceanfishTooltip(inst)
    local tooltip = {}
    table.insert(tooltip, {GetLootTooltip("trophyscale_fish", blueprintschance["oceanfish_trophyscale_fish"], 1)})
    table.insert(tooltip, {GetLootTooltip("fish_box", blueprintschance["oceanfish_fish_box"], 1)})
    return tooltip
end

PrefabSettingLootBlueprints("oceanfish_small_1_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_small_2_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_small_3_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_small_4_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_small_5_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_medium_1_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_medium_2_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_medium_3_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_medium_4_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_medium_5_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_medium_6_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)
PrefabSettingLootBlueprints("oceanfish_medium_7_inv", OceanfishChance, OceanfishLoot, nil, OceanfishTooltip)



-- fill table blueprintschance
if #blueprintschance == 0 then
    for k, v in pairs(lootblueprints) do
        if v.chance_fn ~= nil then
            v.chance_fn()
        end
    end
end

return lootblueprints
