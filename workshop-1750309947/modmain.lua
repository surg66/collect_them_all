env._G = GLOBAL
env.require = _G.require
env.STRINGS = _G.STRINGS

_G.require("recipe")
local cooking = require("cooking")
local PersistentData = require("persistentdata")
local DataContainer = PersistentData("collectthemall")
local LootDropInfo = require "widgets/lootdropinfo"
local lootblueprints = require "lootblueprints"
local settingscharacters = require "cta_stettings_characters"
local cache_tooltips = {}
local lootdropinfo = nil
local show_tooltip = true

DataContainer:Load()

local function EncodePacket(guid, data)
    local result = ""

    if data ~= nil then
        local chance = 0
        local count = 1
        result = result.."g:"..guid
        for i = 1, #data do
            for j = 1, #data[i] do
                if data[i][j].count ~= nil then
                     count = data[i][j].count
                else count = 1 end
                if data[i][j].chance ~= nil then
                     chance = data[i][j].chance
                else chance = 0 end

                result = result.."i:"..data[i][j].item..",c:"..chance..",n:"..count..";"                
            end
            if i < #data then
                result = result.."/"
            end
        end
    end

    return result
end

local function DecodePacket(str)
    if str == nil then return nil end

    local res_guid = nil
    local res_data = nil
    local DP_STATES = {
        NONE = 0,
        READ_GUID = 1,
        READ_ITEM = 2,
        READ_CHANCE = 3,
        READ_COUNT = 4,
        END_BLUEPRINT = 5,
        END_CONTAINER = 6
    }
    local length = #str

    if length > 0 then
        local state = DP_STATES.NONE
        local str_guid = ""
        local str_item = ""
        local str_chance = ""
        local str_count = ""
        local blueprints = {}
        local containers = {}
        local i = 1
        while i <= length do
            local chcode = ""
            local ch     = string.sub(str, i, i)
            if i < length - 1 then chcode = string.sub(str, i, i + 1) end
            if chcode == "g:" then
                state = DP_STATES.READ_GUID
                str_guid = ""
                i = i + 2
            end
            if chcode == "i:" then
                state = DP_STATES.READ_ITEM
                str_item = ""
                i = i + 2
            end
            if chcode == "c:" then
                state = DP_STATES.READ_CHANCE
                str_chance = ""
                i = i + 2
            end
            if chcode == "n:" then
                state = DP_STATES.READ_COUNT
                str_count = ""
                i = i + 2
            end
            if ch == "," then
                state = DP_STATES.NONE
            end
            if ch == ";" then
                state = DP_STATES.END_BLUEPRINT
            end
            if ch == "/" then
                state = DP_STATES.END_CONTAINER
            end
            -- ---------------------------------
            ch = string.sub(str, i, i)

            if state == DP_STATES.READ_GUID then str_guid = str_guid..ch end
            if state == DP_STATES.READ_ITEM then str_item = str_item..ch end
            if state == DP_STATES.READ_CHANCE then str_chance = str_chance..ch end
            if state == DP_STATES.READ_COUNT then str_count = str_count..ch end
            if state == DP_STATES.END_BLUEPRINT then
                table.insert(blueprints, {item = str_item, chance = _G.tonumber(str_chance), count = _G.tonumber(str_count)})
                if i == length then state = DP_STATES.END_CONTAINER end
            end
            if state == DP_STATES.END_CONTAINER then 
                table.insert(containers, blueprints)
                blueprints = {}
            end

            i = i + 1
        end
        if #containers > 0 then
            res_guid = str_guid
            res_data = containers
        end
    end

    return res_guid, res_data
end

for prefab, data in pairs(lootblueprints) do
    if data.postinit_fn ~= nil then
        AddPrefabPostInit(prefab, function(inst)
            if _G.TheWorld.ismastersim then data.postinit_fn(inst) end
        end)
    end
end

for prefab, fn in pairs(settingscharacters) do
    if fn ~= nil then
        AddPrefabPostInit(prefab, function(inst)
            if _G.TheWorld.ismastersim then fn(inst) end
        end)
    end
end

AddPrefabPostInit("blueprint", function(inst)
    if _G.TheWorld.ismastersim then
        if inst.components.hauntable ~= nil then
            inst.components.hauntable.onhaunt = function(inst, haunter) return false end
        end
    end
end)

if not _G.TheNet:IsDedicated() then
    AddClassPostConstruct("widgets/hoverer", function(self)
        local original_OnUpdate = self.OnUpdate
        local original_UpdatePosition = self.UpdatePosition

        self.lootdropinfo = self:AddChild(LootDropInfo())
        lootdropinfo = self.lootdropinfo

        function self:OnUpdate()
            original_OnUpdate(self)

            if show_tooltip then
                local target = _G.TheInput:GetHUDEntityUnderMouse()
                local data = nil
				if target ~= nil then
					target = target.widget ~= nil and target.widget.parent ~= nil and target.widget.parent.item
				else
					target = _G.TheInput:GetWorldEntityUnderMouse()
				end

                if _G.TheWorld.ismastersim then
                    if target ~= nil then
                        local lootblueprint = lootblueprints[target.prefab]
                        if lootblueprint ~= nil and lootblueprint.tooltip_fn ~= nil then
                            data = lootblueprint.tooltip_fn(target)
                        end
                    end
                else
                    local tooltip = nil
                    local timeupdate = _G.GetTime()
                    if target ~= nil then
                        local lootblueprint = lootblueprints[target.prefab]
                        if lootblueprint ~= nil and lootblueprint.tooltip_fn ~= nil then
                            local sendserver = false
                            for i = 1, #cache_tooltips do
                                if cache_tooltips[i].guid == _G.tostring(target.GUID) then
                                    tooltip = cache_tooltips[i]
                                    data = tooltip.data
                                    break
                                end
                            end

                            if tooltip == nil then
                                sendserver = true
                            else
                                timeupdate = _G.GetTime() - tooltip.timeupdate
                                if timeupdate > 2 then sendserver = true end
                            end                    

                            if sendserver then SendModRPCToServer(MOD_RPC.CollectThemAll.Tooltip, target.GUID, target) end
                        end
                    else
                        for i = #cache_tooltips, 1, -1 do
                            timeupdate = _G.GetTime() - cache_tooltips[i].timeupdate
                            if timeupdate > 5 then table.remove(cache_tooltips, i) end
                        end
                    end
                end
                self.lootdropinfo:SetTooltip(target, data)
            end
        end

        function self:UpdatePosition(x, y)
            original_UpdatePosition(self, x, y)

            local scale = self:GetScale()
            local w = 0
            local h = 0

            if self.text ~= nil and self.str ~= nil then
                local w0, h0 = self.text:GetRegionSize()
                w = math.max(w, w0)
                h = math.max(h, h0)
            end
            if self.secondarytext ~= nil and self.secondarystr ~= nil then
                local w1, h1 = self.secondarytext:GetRegionSize()
                w = math.max(w, w1)
                h = math.max(h, h1)
            end

            w = w * scale.x * .5
            h = h * scale.y * .5

            self.lootdropinfo:SetPosition(0, h + 60, 0)
        end
    end)
end

AddModRPCHandler("CollectThemAll", "Tooltip", function(player, guid, target)
    if player.player_classified ~= nil and target ~= nil then
        local lootblueprint = lootblueprints[target.prefab]
        if lootblueprint ~= nil and lootblueprint.tooltip_fn ~= nil then
            local data = EncodePacket(guid, lootblueprint.tooltip_fn(target))
            if data ~= nil then
                player.player_classified.cta_loot_data:set_local(data)
                player.player_classified.cta_loot_data:set(data)
            end
        end
    end
end)

AddPrefabPostInit("player_classified",function(inst)
    inst.cta_loot_data = _G.net_string(inst.GUID, "cta_loot_data", "cta_loot_data_dirty")
    inst:DoTaskInTime(0, function(inst)
        if not _G.TheWorld.ismastersim then
            inst:ListenForEvent("cta_loot_data_dirty", function(inst)
                local guid, decode = DecodePacket(inst.cta_loot_data:value())
                if guid ~= nil then
                    local flag_find = false
                    for i = 1, #cache_tooltips do
                        if cache_tooltips[i].guid == guid then
                            cache_tooltips[i].timeupdate = _G.GetTime()
                            cache_tooltips[i].data = decode
                            flag_find = true
                            break
                        end
                    end
                    if not flag_find then
                    table.insert(cache_tooltips, {timeupdate = _G.GetTime(), guid = guid, data = decode})
                    end
                end
            end)
        end
    end)
end)

local function OnEntityDropLoot(world, data)
    if data ~= nil and data.inst ~= nil and lootblueprints[data.inst.prefab] ~= nil then
        local forcelootdrop_fn = lootblueprints[data.inst.prefab].forcelootdrop_fn
        if forcelootdrop_fn ~= nil then
            forcelootdrop_fn(data.inst)
        end
    end
end

AddPrefabPostInit("world", function(inst)
    if _G.TheWorld.ismastersim then
        inst:ListenForEvent("entity_droploot", OnEntityDropLoot)
    end
end)

AddPrefabPostInit("researchlab", function(inst)
    if _G.TheWorld.ismastersim then inst:RemoveTag("prototyper") end
end)

AddPrefabPostInit("researchlab2", function(inst)
    if _G.TheWorld.ismastersim then inst:RemoveTag("prototyper") end
end)

AddPrefabPostInit("researchlab3", function(inst)
    if _G.TheWorld.ismastersim then inst:RemoveTag("prototyper") end
end)

AddPrefabPostInit("researchlab4", function(inst)
    if _G.TheWorld.ismastersim then inst:RemoveTag("prototyper") end
end)

AddClassPostConstruct("widgets/controls", function(inst)
    show_tooltip = DataContainer:GetValue("VISIBLE_TOOLTIP")

    if show_tooltip == nil then show_tooltip = true end

    if show_tooltip then
        if lootdropinfo ~= nil then lootdropinfo:Hide() end
    else
        if lootdropinfo ~= nil then lootdropinfo:Show() end
    end
end)

AddRecipe("researchlab", {Ingredient("goldnugget", 1),Ingredient("log", 4),Ingredient("rocks", 4)}, nil, _G.TECH.LOST, nil, nil, true)
AddRecipe("researchlab2", {Ingredient("boards", 4),Ingredient("cutstone", 2), Ingredient("transistor", 2)}, nil, _G.TECH.LOST, nil, nil, true)
AddRecipe("researchlab3", {Ingredient("livinglog", 3),Ingredient("purplegem", 1),Ingredient("nightmarefuel", 7)}, nil, _G.TECH.LOST, nil, nil, true)
AddRecipe("researchlab4", {Ingredient("rabbit", 4),Ingredient("boards", 4),Ingredient("tophat", 1)}, nil, _G.TECH.LOST, nil, nil, true)

AddRecipe("sewing_tape", {Ingredient("silk", 5), Ingredient("rope", 2), Ingredient("houndstooth", 1)}, _G.CUSTOM_RECIPETABS.ENGINEERING, _G.TECH.NONE, nil, nil, nil, nil, "handyperson")
AddRecipe("spear_wathgrithr", {Ingredient("twigs", 5), Ingredient("flint", 5), Ingredient("goldnugget", 5)}, _G.RECIPETABS.WAR, _G.TECH.NONE, nil, nil, nil, nil, "valkyrie")
AddRecipe("wathgrithrhat", {Ingredient("goldnugget", 5), Ingredient("rocks", 5)}, _G.RECIPETABS.WAR, _G.TECH.NONE, nil, nil, nil, nil, "valkyrie")

AddComponentPostInit("giftreceiver", function(self)
    local original_SetGiftMachine = self.SetGiftMachine
    self.SetGiftMachine = function(self, inst)
        local pos = self.inst:GetPosition()
        local ents = _G.TheSim:FindEntities(pos.x, pos.y, pos.z, _G.TUNING.RESEARCH_MACHINE_DIST, { "giftmachine", "structure" }, { "INLIMBO", "burnt" })
        local gift_inst = nil
        for _,ent in ipairs(ents) do
            gift_inst = ent
            break
        end
        inst = gift_inst ~= nil and gift_inst:HasTag("giftmachine") and _G.CanEntitySeeTarget(self.inst, gift_inst) and self.inst.components.inventory.isopen and gift_inst or nil
        original_SetGiftMachine(self, inst)
    end
end)

AddPrefabPostInit("firepit", function(inst)
    if not _G.TheWorld.ismastersim then
        return inst
    end

    inst:AddTag("giftmachine")
end)

AddPrefabPostInit("portablecookpot", function(inst)
    if not _G.TheWorld.ismastersim then
        return inst
    end

    inst.components.stewer.isspecialforwarly = true
end)

AddPrefabPostInit("portablespicer", function(inst)
    if not _G.TheWorld.ismastersim then
        return inst
    end

    local onstartcookingfn = inst.components.stewer.onstartcooking

    inst.components.stewer.onstartcooking = function(sinst)
        onstartcookingfn(sinst)
        local prefab = sinst.components.container.slots[1]

        if prefab:HasTag("specialforwarly") then
            sinst.components.stewer.isspecialforwarly = true
        else
            sinst.components.stewer.isspecialforwarly = false
        end
    end
end)

AddComponentPostInit("stewer", function(self)
    -- override Harvest
    self.Harvest = function(self, harvester)
        if self.done then
            if self.onharvest ~= nil then
                self.onharvest(self.inst)
            end

            if self.product ~= nil then
                local loot = _G.SpawnPrefab(self.product)
                if loot ~= nil then
                    local recipe = cooking.GetRecipe(self.inst.prefab, self.product)
                    local stacksize = recipe and recipe.stacksize or 1
                    if stacksize > 1 then
                        loot.components.stackable:SetStackSize(stacksize)
                    end
                
                    if self.spoiltime ~= nil and loot.components.perishable ~= nil then
                        local spoilpercent = self:GetTimeToSpoil() / self.spoiltime
                        loot.components.perishable:SetPercent(self.product_spoilage * spoilpercent)
                        loot.components.perishable:StartPerishing()
                    end

                    if self.isspecialforwarly then
                        loot:AddTag("specialforwarly")
                    end

                    if harvester ~= nil and harvester.components.inventory ~= nil then
                        harvester.components.inventory:GiveItem(loot, nil, self.inst:GetPosition())
                    else
                        LaunchAt(loot, self.inst, nil, 1, 1)
                    end
                end
                self.product = nil
            end

            if self.task ~= nil then
                self.task:Cancel()
                self.task = nil
            end
            self.targettime = nil
            self.done = nil
            self.spoiltime = nil
            self.product_spoilage = nil

            if self.inst.components.container ~= nil then      
                self.inst.components.container.canbeopened = true
            end

            return true
        end
    end
end)

AddComponentPostInit("eater", function(self)
    local original_Eat = self.Eat
    self.Eat = function(self, food, feeder)
        feeder = feeder or self.inst
        if food:HasTag("specialforwarly") then
            if feeder.prefab == "warly" then
                return original_Eat(self, food, feeder)
            else
                return false
            end
        else
            return original_Eat(self, food, feeder)
        end
    end
end)


local original_TEACH = _G.ACTIONS.TEACH.fn
_G.ACTIONS.TEACH.fn = function(act)
    local ret, reason = original_TEACH(act)
    if ret and act.invobject.components.teacher then
        local blueprint_str = act.invobject.name
        if blueprint_str:sub(-9):lower() == "blueprint" then
            blueprint_str = blueprint_str:sub(1,-11)
        end
        local a_or_an = string.find(blueprint_str, "^[aeoiuAOIEU]") ~= nil and "an" or "a"
        _G.TheNet:Announce(string.format("%s now knows how to make %s %s!", act.doer:GetDisplayName(), a_or_an, blueprint_str))
    end
    return ret, reason
end

_G.TheInput:AddKeyDownHandler(_G.KEY_H, function(key)
    if _G.ThePlayer ~= nil and not _G.ThePlayer.HUD:IsChatInputScreenOpen() then
        if show_tooltip then
            show_tooltip = false
            if lootdropinfo ~= nil then lootdropinfo:Hide() end
        else
            show_tooltip = true
            if lootdropinfo ~= nil then lootdropinfo:Show() end
        end
        DataContainer:SetValue("VISIBLE_TOOLTIP", show_tooltip)
        DataContainer:Save()
    end
end)
