# Desctiption
Collect them all - Don't Starve Together modification.

In this mode are excluded: science machine, alchemical engine, prestihatitator, shadow manipulator.
The blueprints could be obtained by killing mobs and bosses, collecting herbs, branches, berries, etc. Every mob, boss or action has increased chance of certain blueprint and small chance of a random blueprint.
For example, a thermal-stone blueprint could be dropped by killing rock lobsters, a chest blueprint drops from treeguards or harvesting bananas, and an ice box blueprint 100% dropping from toadstools. To collect all the recipes you have to fight with all mobs and bosses in the game or rely on incredible luck.
You can open a gift near firepit.
Press key "H" show/hide tooltip.

P.S. Wickerbottom has changed. The character is not able to craft science items. However, she can craft books and papyrus.

[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1750309947)